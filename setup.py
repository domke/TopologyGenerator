#!/usr/bin/env python

from distutils.core import setup
from setuptools import find_packages

setup(name='TopologyGenerator',
      version='0.1.1',
      author='Jens Domke',
      author_email='domke.j.aa@m.titech.ac.jp',
      url='https://gitlab.com/u/domke',
      description='Tool to create/modify various types and sizes of network topologies for usage in network simulation environments',
      long_description=open('README.md').read(),
      license='BSD 3-Clause License (New BSD)',
      packages=find_packages(exclude=['']),
      )
