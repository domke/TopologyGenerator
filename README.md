# TopologyGenerator

Tool to create/modify various types and sizes of network topologies for usage
in network simulation environments

## Dependency

Parser for output of InfiniBand tools (https://gitlab.com/domke/ParseInfiniband)

## Installation

```bash
git clone https://gitlab.com/domke/TopologyGenerator.git
cd TopologyGenerator
pip install --user .
```

## Usage

Simply:

```bash
cd TopologyGenerator
./bin/topo-gen.py --help
```

## Supported topologies

Currently implemented:
- k-ary n-tree
- eXtended Generalized Fat Tree
- Fat Tree (binary tree with more links towards root)
- Dragonfly: Fully-connected, Cascade
- n-dimensional Mesh and Express Mesh
- n-dimensional Torus
- Tofu (used in K-supercomputer)
- Ring
- Hypercube / Enhanced Hypercube (EHC)
- HyperX
- Kautz graph
- Arrangement graph
- random / Jellyfish
- loading existing topologies from DOT files
- loading existing topologies from ibnetdiscover file
- converting between DOT and ibnetdiscover

Planed:
- fault injection, to alter generated/loaded topologies
