#!/usr/bin/env python3

from sys import exit, stdout, stderr
from argparse import ArgumentParser
from os import path, getcwd
from colorsys import hsv_to_rgb
from contextlib import contextmanager
from re import compile, sub
from hashlib import md5
from io import IOBase
from ..common import _topology_is_valid, _extract_basic_config
from ..common import _natural_sort
from ..common import _topology_port_matching, _topology_is_port_matched
from ..common import _topology_port_matching_symmetrical
from ..common import _topology_remove_port_matching
from .infiniband import _get_local_options as _get_ibnet_options


@contextmanager
def __smart_open(filename=None):
    if filename and filename != '-':
        fh = open(filename, 'w')
    else:
        fh = stdout

    try:
        yield fh
    finally:
        if fh is not stdout:
            fh.close()


def _set_local_options(arg_parser):
    if not arg_parser or not isinstance(arg_parser, ArgumentParser):
        exit('ERR: needs to be called with existing ArgumentParser object')

    arg_group = arg_parser.add_argument_group(
        'DOT format (dot) output options')

    arg_group.add_argument(
        '--dap',
        dest='__dot_add_ports__',
        action='store_true',
        help='add port information for adjacent nodes',
        default=False)
    arg_group.add_argument(
        '--dic',
        dest='__dot_include_colors__',
        action='store_true',
        help='add link colors based on InfiniBand width/speed (helpful to see inhomogeneous link configurations)',
        default=False)


def _get_local_options(args):
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    do_port_matching = args.get('__dot_add_ports__')
    include_link_colors = args.get('__dot_include_colors__')

    return (do_port_matching, include_link_colors)


def _read_dot_graph(dot, topology, args={}):
    if not isinstance(dot, IOBase) or not isinstance(topology, dict):
        exit('ERR: invalid function parameter type(s)')

    exit('ERR: reading undirected graphs not supported, yet')


def _read_dot_digraph(dot, topology, args={}):
    if not isinstance(dot, IOBase) or not isinstance(topology, dict):
        exit('ERR: invalid function parameter type(s)')

    ports = {}
    post_mortem_port_matching = False

    _, num_ports_per_terminal, terminal_prefix, \
        num_ports_per_switch, switch_prefix = _extract_basic_config(args)

    if num_ports_per_terminal == -1:
        num_ports_per_terminal = 1

    # if '[comment=' exists, then the node guid MUST be the first after '='
    node_re = compile(
        r'^\s*"{0,1}([\w\s]+)"{0,1}\s*(\[?\s*comment="([\w;,>=\-]*)"\s*\]|([#/;]+.*|$))')
    link_re = compile(
        r'^\s*"{0,1}([\w\s]+)"{0,1}\s*[>\-]{2}\s*"{0,1}([\w\s]+)"{0,1}\s*(\[?\s*comment="([\w;,>=\-\?]*)"\s*\]|([#/;]+.*|$))')
    ports_re = compile(r'P(\d+)\s*->\s*P([\?\d]+)')

    in_multi_line_comment = False
    nesting_level = 1

    for line in dot:
        # strip single-line comments from line and lines starting with '#'
        line = sub(r'^\s*#.*$|//.*$|/\*.*\*/', '', line)
        # ignore multi-line comments, too
        if line.count('/*') > 0:
            in_multi_line_comment = True
            continue
        elif in_multi_line_comment:
            if line.count('*/') > 0:
                line = sub(r'^.*\*/', '', line)
            else:
                continue
        # check '{' and '}' to see when the graph definition is over
        nesting_level += (line.count('{') - line.count('}'))
        if nesting_level == 0:
            break

        # see if the line contains a node definition
        if node_re.match(line):
            m = node_re.match(line)
            node = m.group(1)
            node_comment = m.group(3)
            try:
                node_guid = int(node_comment.split(',')[0].split(';')[0], 16)
            except:
                node_guid = (
                    int(md5(node.encode('utf-8')).hexdigest(), 16) %
                    pow(2, 64))

            if node in topology:
                exit(
                    'ERR: incompatible DOT format, node definition (for %s) in DOT found after edge definition' %
                    node)

            if node.find(terminal_prefix) == 0:
                topology[node] = {'guid': node_guid,
                                  'radix': num_ports_per_terminal,
                                  'adj': []}
                ports[node] = num_ports_per_terminal
            elif node.find(switch_prefix) == 0:
                topology[node] = {'guid': node_guid,
                                  'radix': num_ports_per_switch,
                                  'adj': [],
                                  'is_switch': True}
                ports[node] = num_ports_per_switch
            else:
                exit(
                    'ERR: incompatible DOT format, node names must start with \'%s\' for terminals, or \'%s\' for switches' %
                    (terminal_prefix, switch_prefix))

            if 'root_switch' in node_comment:
                topology[node]['is_root'] = True
            if 'radix' in node_comment:
                topology[node]['radix'] = int(
                    compile(r'.*radix\s*=\s*(\d+)').match(node_comment).group(1))
                if num_ports_per_switch == -1:
                    ports[node] = topology[node]['radix']
        # or a link definition
        elif link_re.match(line):
            m = link_re.match(line)
            loc_node = m.group(1)
            rem_node = m.group(2)
            link_comment = m.group(4)

            # if one of the nodes isn't there yet, we assume it is a terminal
            if loc_node not in topology:
                node_guid = (
                    int(md5(loc_node.encode('utf-8')).hexdigest(), 16) %
                    pow(2, 64))
                topology[loc_node] = {'guid': node_guid,
                                      'radix': num_ports_per_terminal,
                                      'adj': []}
                ports[node] = num_ports_per_terminal
            if rem_node not in topology:
                node_guid = (
                    int(md5(rem_node.encode('utf-8')).hexdigest(), 16) %
                    pow(2, 64))
                topology[rem_node] = {'guid': node_guid,
                                      'radix': num_ports_per_terminal,
                                      'adj': []}
                ports[node] = num_ports_per_terminal

            # check if port numbers have been provided
            if ports_re.match(link_comment):
                m = ports_re.match(link_comment)
                lport = int(m.group(1))
                if '?' in m.group(2):
                    rport = -1
                    post_mortem_port_matching = True
                else:
                    rport = int(m.group(2))
                topology[loc_node]['adj'].append([lport, rem_node, rport])
            else:
                topology[loc_node]['adj'].append(rem_node)
                post_mortem_port_matching = True

            # in this format we have the link twice, so substracting one from
            # ports at remote node is not needed
            ports[loc_node] -= 1

    # fix the ports dict if previously the switch radix was unknown
    for sw in topology:
        if topology[sw]['radix'] == -1:
            topology[sw]['radix'] = len(topology[sw]['adj'])
            ports[sw] += (topology[sw]['radix'] + 1)

    # sometimes... ok, lets do the port matching here if the devs are too lazy
    if post_mortem_port_matching:
        _topology_port_matching_symmetrical(topology)

    # is the topology valid?
    if len(topology) < 1:
        stderr.write('WRN: no nodes/link found in the DOT file\n')
    if not _topology_is_valid(topology, ports, switch_prefix):
        exit('ERR: invalid digraph provided')


def _read_dot_topology(input_file, topology, preserve_ports=False, args={}):
    if not isinstance(input_file, str) or not isinstance(topology, dict):
        exit('ERR: invalid function parameter type(s)')

    with open(input_file, 'r') as dot:
        for line in dot:
            if compile(r'^\s*graph\s*{').match(line):
                _read_dot_graph(dot, topology, args)
                break
            elif compile(r'^\s*digraph\s*{').match(line):
                # read the topology from file
                _read_dot_digraph(dot, topology, args)
                # and now clean-up if port matching isn't relevant for later
                if not preserve_ports:
                    _topology_remove_port_matching(topology)
                break


def _get_link_color(width, speed):
    if speed == 'SDR':
        base = 240.0
    elif speed == 'DDR':
        base = 216.0
    elif speed == 'QDR':
        base = 180.0
    elif speed == 'FDR10':
        base = 144.0
    elif speed == 'FDR':
        base = 108.0
    elif speed == 'EDR':
        base = 72.0
    elif speed == 'HDR':
        base = 36.0
    elif speed == 'NDR':
        base = 0.0
    else:
        return None

    return base + ((width - 1) / 11 * 35)


def _write_dot_file(output_file, topology, port_matched_topology,
                    supplement_material={}, link_width=None, link_speed=None):
    if not isinstance(output_file, str) or not isinstance(
            topology, dict) or not isinstance(port_matched_topology, bool):
        exit('ERR: invalid function parameter type(s)')

    have_irregular_links = False
    irregular_links = supplement_material.get('irregular_links')
    if irregular_links and isinstance(irregular_links, dict):
        have_irregular_links = True

    # prepare lookup table to only write links from 'low' to 'high' indexed
    # node
    if port_matched_topology:
        node_list = list(topology.keys())

    with __smart_open(output_file) as out:
        if output_file != '':
            out.write('// try: neato -Tsvg %s -o %s.svg\n' %
                      (output_file, output_file))
        out.write('graph {\n')

        # first write the nodes and additional information
        for node in sorted(topology.keys(), key=_natural_sort):
            comment = []
            if 'guid' in topology[node]:
                comment.append('0x%016x' % topology[node]['guid'])
            if topology[node].get('is_root'):
                comment.append('root_switch')
            if 'radix' in topology[node]:
                comment.append('radix=%i' % topology[node]['radix'])
            out.write('    "%s" [comment="%s"];\n' % (node, ','.join(comment)))

        # now we write the links of our topology
        for node in sorted(topology.keys(), key=_natural_sort):
            for link in topology[node]['adj']:
                # unfold the link into its components
                if isinstance(link, str):
                    lport, remote_node, rport = None, link, None
                elif isinstance(link, list) and len(link) >= 3:
                    if len(link) == 3:
                        lport, remote_node, rport = link
                    elif len(link) == 4:
                        lport, remote_node, rport, _ = link
                    if rport < 0:
                        rport = '?'
                # check if we have to write this link, or if it was done before
                if port_matched_topology:
                    if node_list.index(node) > node_list.index(remote_node):
                        continue

                comment = []
                if lport is not None and rport is not None:
                    comment.append('P%s->P%s' % (lport, rport))
                if link_width is not None and link_speed is not None:
                    comment.append('%sx%s' % (link_width, link_speed))

                buffer = '    "%s" -- "%s" [comment="%s"];' % (
                    node, remote_node, ','.join(comment))

                lwidth, lspeed = link_width, link_speed
                if have_irregular_links:
                    # check if regular or irregular link
                    edge = '%s--%s' % (node, remote_node)
                    rev_edge = '%s--%s' % (remote_node, node)
                    if edge in irregular_links:
                        lwidth, lspeed = irregular_links[edge]
                    elif rev_edge in irregular_links:
                        lwidth, lspeed = irregular_links[rev_edge]

                # color the link if desired
                color_code = _get_link_color(lwidth, lspeed)
                if color_code is not None:
                    rgb = hsv_to_rgb(color_code / 360.0, 1.0, 1.0)
                    color = '#%02x%02x%02x' % (
                        255 * rgb[0],
                        255 * rgb[1],
                        255 * rgb[2])
                    buffer = '    { edge [color="%s"]\n    %s\n    }' % (
                        color, buffer)

                out.write('%s\n' % buffer)

        out.write('}\n')


def _write_dot_topology(topology, args={}, supplement_material={}):
    if not isinstance(args, dict) or not isinstance(topology, dict):
        exit('ERR: invalid function parameter type(s)')

    output_file = args.get('output_file')
    if output_file is None:
        output_file = ''

    # inofficially use ibnet configs to color the links in the DOT file,
    # otherwise if we are in library mode then links are just black
    if args.get('__argparse_used__'):
        do_port_matching, include_link_colors = _get_local_options(args)
        lwidth, lspeed, _ = _get_ibnet_options(args)
    else:
        lwidth, lspeed = None, None

    port_matched_topology = _topology_is_port_matched(args)
    if not port_matched_topology and do_port_matching:
        _topology_port_matching(topology)
        port_matched_topology = True

    if not include_link_colors:
        lwidth, lspeed = None, None

    _write_dot_file(output_file,
                    topology,
                    port_matched_topology,
                    supplement_material,
                    lwidth,
                    lspeed)


if __name__ == "__main__":
    exit('ERR: not meant to be executed as stand-alone')
