#!/usr/bin/env python3

from sys import exit, stdout, stderr
from math import ceil
from itertools import chain
from random import shuffle
from os import getenv
from networkx.algorithms.community.kernighan_lin import kernighan_lin_bisection as bisection
from networkx.algorithms.boundary import edge_boundary
from networkx.algorithms.distance_measures import diameter, radius
from networkx.classes import MultiGraph
from .dot import _read_dot_topology as read_dot
from .dot import _write_dot_topology as write_dot
from .infiniband import _read_ibnet_topology as read_ibnet
from .infiniband import _write_ibnet_topology as write_ibnet


def _try_cluster_into_racks_with_recursive_bisection(G, in_partition,
                                                     num_links, cluster_size):
    if not isinstance(G, MultiGraph) or not isinstance(num_links, int) or \
            not isinstance(cluster_size, int) or not cluster_size >= 2:
        exit('ERR: invalid function parameter type(s)')

    if cluster_size >= len(list(G.nodes())):
        return [list(G.nodes())]

    left_p, right_p = bisection(G,
                                partition=in_partition,
                                max_iter=max(10, ceil(1e9/num_links)))
    return \
        _try_cluster_into_racks_with_recursive_bisection(G.subgraph(left_p),
                                                         None, num_links,
                                                         cluster_size) + \
        _try_cluster_into_racks_with_recursive_bisection(G.subgraph(right_p),
                                                         None, num_links,
                                                         cluster_size)


def _flatten(nested_list):
    if not isinstance(nested_list, list):
        exit('ERR: invalid function parameter type(s)')
    in_list = list(chain.from_iterable(nested_list))
    # remove duplicates
    seen = set()
    seen_add = seen.add
    return [e for e in in_list if not (e in seen or seen_add(e))]


def _dump_topo(topology):
    if not isinstance(topology, dict):
        exit('ERR: invalid function parameter type(s)')

    for node in topology:
        stdout.write('%s: %s\n' % (node, topology[node]))


def _dump_suppl(supplement_material):
    if not isinstance(supplement_material, dict):
        exit('ERR: invalid function parameter type(s)')

    for entry in supplement_material:
        stdout.write('%s: %s\n' % (entry, supplement_material[entry]))


def _dump_stats(args={}, topology={}):
    if not isinstance(args, dict) or not isinstance(topology, dict):
        exit('ERR: invalid function parameter type(s)')

    sw_pref = args.get('switch_prefix')
    term_pref = args.get('terminal_prefix')
    verbose = args.get('verbose')
    if not sw_pref or not term_pref or not verbose:
        return

    num_switches = len([sw for sw in topology
                        if topology[sw].get('is_switch')
                        or sw.find(sw_pref) == 0])
    num_terminals = len([term for term in topology
                         if term.find(term_pref) == 0
                         or not topology[term].get('is_switch')])
    stderr.write('Topology statistics:\n')
    stderr.write(f'\t#switches: {num_switches}\n')
    stderr.write(f'\t#terminals: {num_terminals}\n')
    num_links = 0
    for sw in topology:
        if topology[sw].get('is_switch') or sw.find(sw_pref) == 0:
            num_links += len([adj for adj in topology[sw]['adj']
                              if topology[adj].get('is_switch')
                              or adj.find(sw_pref) == 0])
    stderr.write(f'\t#sw2sw-links: {num_links}\n')
    G, in_groups, sw_list, sw_leaf_list = MultiGraph(), [], [], set()
    for sw in topology:
        if topology[sw].get('is_switch') or sw.find(sw_pref) == 0:
            sw_guid = topology[sw]['guid']
            in_groups.append([sw_guid])
            sw_list.append(sw_guid)
            for adj in topology[sw]['adj']:
                adj_guid = topology[adj]['guid']
                if topology[adj].get('is_switch') or adj.find(sw_pref) == 0:
                    G.add_edge(sw_guid, adj_guid)
                elif adj.find(term_pref) == 0 or not topology[adj].get('is_switch'):
                    G.add_edge(sw_guid, adj_guid)
                    # put all term in same bucket as their parent (last added)
                    in_groups[-1].append(adj_guid)
                    sw_leaf_list.add(sw_guid)

    if False:
        # XXX: only for internal purpose, usually incorrect if #term < #leaf
        num_ports_per_switch = args.get('num_ports_per_switch')
        num_ports_per_terminal = args.get('num_ports_per_terminal')
        num_unpopulated_terminals = 0
        for sw_guid in sw_leaf_list:
            num_unpopulated_terminals += (num_ports_per_switch - G.degree[sw_guid]) // num_ports_per_terminal
        stderr.write(f'\tmax terminals: {num_terminals+num_unpopulated_terminals}\n')

    num_bisec_links = 1e100
    for i in range(1000):
        f_in_groups = _flatten(in_groups)
        in_partition = (set(f_in_groups[:len(f_in_groups)//2]),
                        set(f_in_groups[len(f_in_groups)//2:]))
        left_part, right_part = bisection(G,
                                          partition=in_partition,
                                          max_iter=max(10,
                                                       ceil(1e9/num_links)),
                                          seed=i)
        num_bisec_links = min(num_bisec_links,
                              len(list(edge_boundary(G,
                                                     left_part, right_part))))
        shuffle(in_groups)

    stderr.write(f'\tsw2sw degrees: ['
                 f'{min(dict(G.degree(sw_list)).values())}, '
                 f'{max(dict(G.degree(sw_list)).values())}]\n')
    stderr.write(f'\tdiameter: {diameter(G)}\n')
    stderr.write(f'\tradius: {radius(G)}\n')
    stderr.write(f'\tapprox. bisection BW: {num_bisec_links}\n')

    if getenv('NUMBER_SWITCH_RACKS'):
        # assume: all terminals have short cables to in-rack or neighbor switch
        # assume: one can cable with short inside racks and long among them
        clusters = _try_cluster_into_racks_with_recursive_bisection(
            G.subgraph(sw_list),
            (set(left_part) & set(sw_list), set(right_part) & set(sw_list)),
            num_links,
            int(getenv('NUMBER_SWITCH_RACKS')))
        num_links = len(list(G.edges()))
        short_links = num_links - len(list(G.subgraph(sw_list).edges()))
        for nodes in clusters:
            short_links += len(list(G.subgraph(nodes).edges()))
        stderr.write(f'\tapprox. #short copper: {short_links}\n')
        stderr.write(f'\tapprox. #longer AOC: {num_links - short_links}\n')

    del G, left_part, right_part, in_groups, sw_list, sw_leaf_list


def _read(topology, input_file, input_type, input_rootguid_file=None, args={}):
    if not isinstance(topology, dict) or not isinstance(
            input_file, str) or not isinstance(input_type, str):
        exit('ERR: invalid function parameter type(s)')

    # if we are reading/loading a topology, then we might wanna preserve the
    # port information in case the topology is written out again
    preserve_ports = False
    if args.get('include_ports') or args.get('__argparse_used__'):
        preserve_ports = True

    if input_type == 'dot':
        read_dot(input_file, topology, preserve_ports, args)
    elif input_type == 'ibnet':
        read_ibnet(input_file,
                   topology,
                   input_rootguid_file,
                   preserve_ports,
                   args)
    else:
        exit('ERR: unknown/unsupported input type')


def write(topology, args={}, supplement_material={}):
    if not isinstance(topology, dict) or not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    output_type = args.get('output_type')
    if output_type is None:
        output_type = 'ibnet'

    _dump_stats(args, topology)

    if False:  # TODO: take out later
        _dump_topo(topology)
        _dump_suppl(supplement_material)

    if output_type == 'dot':
        write_dot(topology, args, supplement_material)
    elif output_type == 'ibnet':
        write_ibnet(topology, args, supplement_material)
    else:
        exit('ERR: unknown/unsupported output format')


if __name__ == "__main__":
    exit('ERR: not meant to be []executed as stand-alone')
