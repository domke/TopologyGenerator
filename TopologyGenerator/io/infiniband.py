#!/usr/bin/env python3

from sys import exit, stdout, stderr
from argparse import ArgumentParser
from os import path, getcwd
from contextlib import contextmanager
from re import compile
from ParseInfiniband.ibnetdiscover import read as read_ibnet
from ..common import _topology_is_valid, _extract_basic_config
from ..common import _natural_sort
from ..common import _topology_port_matching, _topology_is_port_matched
from ..common import _topology_remove_port_matching


@contextmanager
def __smart_open(filename=None):
    if filename and filename != '-':
        fh = open(filename, 'w')
    else:
        fh = stdout

    try:
        yield fh
    finally:
        if fh is not stdout:
            fh.close()


def _set_local_options(arg_parser):
    if not arg_parser or not isinstance(arg_parser, ArgumentParser):
        exit('ERR: needs to be called with existing ArgumentParser object')

    arg_group = arg_parser.add_argument_group(
        'InfiniBand (ibnet) output options')

    arg_group.add_argument(
        '--wid',
        dest='__ibnet_link_width__',
        type=int,
        help='link width for the IB network [default: 4]',
        choices=[1, 2, 4, 8, 12],
        default=None)
    arg_group.add_argument(
        '--spe',
        dest='__ibnet_link_speed__',
        help='link speed for the IB network [default: QDR]',
        choices=['SDR', 'DDR', 'QDR', 'FDR10', 'FDR', 'EDR', 'HDR', 'NDR'],
        default=None)
    arg_group.add_argument(
        '--esp0',
        dest='__ibnet_enhanced_switch_port__',
        action='store_true',
        help='switches should be configured to have an enhanced switch port 0',
        default=False)


def _get_local_options(args):
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    link_width = args.get('__ibnet_link_width__')
    link_speed = args.get('__ibnet_link_speed__')
    esp0 = args.get('__ibnet_enhanced_switch_port__')

    return (link_width, link_speed, esp0)


def _read_ibnet_topology(input_file, topology, input_rootguid_file=None,
                         preserve_ports=False, args={}):
    if not isinstance(input_file, str) or not isinstance(topology, dict):
        exit('ERR: invalid function parameter type(s)')

    ports = {}

    _, num_ports_per_terminal, _, \
        num_ports_per_switch, _ = _extract_basic_config(args)

    guid_re = compile(r'^\s*0x(\w+)')

    root_guids = []
    if input_rootguid_file:
        with open(input_rootguid_file, 'r') as rguids:
            for line in rguids:
                if guid_re.match(line):
                    m = guid_re.match(line)
                    try:
                        guid = int(m.group(1), 16)
                        root_guids.append(guid)
                    except:
                        continue

    network, sw_list, ca_list = read_ibnet(
        input_file, elements='NodeID,Ports,ESP0,LinkSpeed')
    node_list = sw_list + ca_list

    # move all switches over to the topology dict
    for sw_guid in sw_list:
        switch = network[sw_guid]['NodeID']
        if num_ports_per_switch == -1:
            sw_ports = network[sw_guid]['Ports']
        else:
            sw_ports = num_ports_per_switch
        topology[switch] = {'guid': int(sw_guid, 16),
                            'radix': sw_ports,
                            'adj': [],
                            'is_switch': True}
        if network[sw_guid].get('ESP0'):
            topology[switch]['esp0'] = True
        ports[switch] = topology[switch]['radix']

    # and now move all CAs over
    for ca_guid in ca_list:
        terminal = network[ca_guid]['NodeID']
        if num_ports_per_terminal > network[ca_guid]['Ports']:
            term_ports = num_ports_per_terminal
        else:
            term_ports = network[ca_guid]['Ports']
        topology[terminal] = {'guid': int(ca_guid, 16),
                              'radix': term_ports,
                              'adj': []}

    # and finally add all outgoing links
    for lguid in node_list:
        lnode = network[lguid]['NodeID']
        for pn in sorted(network[lguid]['PN'].keys()):
            rguid = network[lguid]['PN'][pn]['NodeGUID']
            rport = network[lguid]['PN'][pn]['PN'] - 1
            rnode = network[rguid]['NodeID']
            # network stores a symmetrical representation -> process rnode
            # later
            topology[lnode]['adj'].append([pn - 1, rnode, rport])
            if network[lguid]['PN'][pn].get('LinkSpeed'):
                topology[lnode][
                    'adj'][-1].append(network[lguid]['PN'][pn]['LinkSpeed'])
            if topology[lnode].get('is_switch'):
                ports[lnode] -= 1

    # translate list of rootguids to list of NodeIDs, and set root switch info
    root_switches = [network['%016x' % guid]['NodeID'] for guid in root_guids]
    if len(root_switches) == 0:
        topology[network[sw_list[0]]['NodeID']]['fake_root_switch'] = True
    else:
        for switch in root_switches:
            topology[switch]['is_root'] = True

    # is the topology valid?
    if len(topology) < 1:
        stderr.write('WRN: no nodes/link found in the ibnet file\n')
    if not _topology_is_valid(topology, ports):
        exit('ERR: invalid ibnet input file (switch radix < number of links)')

    # finally clean-up if the port matching isn't relevant for later
    if not preserve_ports:
        _topology_remove_port_matching(topology)


def _write_osm_rootguid_file(output_file, topology):
    if not isinstance(output_file, str) or not isinstance(topology, dict):
        exit('ERR: invalid function parameter type(s)')

    with __smart_open(output_file) as out:
        for root_sw in [sw for sw in topology if topology[sw].get('is_root')]:
            out.write('0x%016x\n' % topology[root_sw]['guid'])

        for root_sw in [sw for sw in topology if topology[
                sw].get('fake_root_switch')]:
            out.write('0x%016x\n' % topology[root_sw]['guid'])


def _write_osm_dor_config_file(output_file, topology, dor):
    if not isinstance(output_file, str) or not isinstance(topology, dict):
        exit('ERR: invalid function parameter type(s)')

    with __smart_open(output_file) as out:
        for switch in dor:
            out.write('0x%016x' % topology[switch]['guid'])
            prev_lport, prev_rnode = -1, ''
            for neighbor in dor[switch]['dimList']:
                for link in topology[switch]['adj']:
                    if not isinstance(link, list) or len(link) != 3:
                        exit(
                            'ERR: unsupported or pre-port-matching link format')
                    lport, rnode, rport = link
                    if neighbor == rnode and (
                            prev_rnode != rnode or prev_lport != lport):
                        # IB starts port numbers with 1, so add 1
                        out.write(' %s' % (lport + 1))
                        prev_lport, prev_rnode = lport, rnode
                        break
            out.write('\n')


def _write_osm_torus2qos_config_file(output_file, topology, torus2qos):
    if not isinstance(output_file, str) or not isinstance(
            topology, dict) or not isinstance(torus2qos, dict):
        exit('ERR: invalid function parameter type(s)')

    with __smart_open(output_file) as out:
        out.write(
            '%s %s %s %s\n' % (torus2qos['topo'],
                               torus2qos['xradix'],
                               torus2qos['yradix'],
                               torus2qos['zradix']))
        out.write(
            'portgroup_max_ports %s\n' %
            torus2qos['portgroup_max_ports'])
        round = 0
        for seed_sw in torus2qos['seed']:
            if round > 0:
                out.write('next_seed\n')

            if 'xp_link' in torus2qos['seed'][seed_sw]:
                out.write(
                    'xp_link 0x%016x 0x%016x\n' %
                    (topology[seed_sw]['guid'],
                     topology[torus2qos['seed'][seed_sw]['xp_link']]['guid']))
            if 'xm_link' in torus2qos['seed'][seed_sw]:
                out.write(
                    'xm_link 0x%016x 0x%016x\n' %
                    (topology[seed_sw]['guid'],
                     topology[torus2qos['seed'][seed_sw]['xm_link']]['guid']))
            if 'yp_link' in torus2qos['seed'][seed_sw]:
                out.write(
                    'yp_link 0x%016x 0x%016x\n' %
                    (topology[seed_sw]['guid'],
                     topology[torus2qos['seed'][seed_sw]['yp_link']]['guid']))
            if 'ym_link' in torus2qos['seed'][seed_sw]:
                out.write(
                    'ym_link 0x%016x 0x%016x\n' %
                    (topology[seed_sw]['guid'],
                     topology[torus2qos['seed'][seed_sw]['ym_link']]['guid']))
            if 'zp_link' in torus2qos['seed'][seed_sw]:
                out.write(
                    'zp_link 0x%016x 0x%016x\n' %
                    (topology[seed_sw]['guid'],
                     topology[torus2qos['seed'][seed_sw]['zp_link']]['guid']))
            if 'zm_link' in torus2qos['seed'][seed_sw]:
                out.write(
                    'zm_link 0x%016x 0x%016x\n' %
                    (topology[seed_sw]['guid'],
                     topology[torus2qos['seed'][seed_sw]['zm_link']]['guid']))
            if 'x_dateline' in torus2qos['seed'][seed_sw]:
                out.write(
                    'x_dateline %s\n' %
                    torus2qos['seed'][seed_sw]['x_dateline'])
            if 'y_dateline' in torus2qos['seed'][seed_sw]:
                out.write(
                    'y_dateline %s\n' %
                    torus2qos['seed'][seed_sw]['y_dateline'])
            if 'z_dateline' in torus2qos['seed'][seed_sw]:
                out.write(
                    'z_dateline %s\n' %
                    torus2qos['seed'][seed_sw]['z_dateline'])

            round += 1


def _conv_lspeed(speed):
    if speed == 'SDR':
        return [1, 0]
    elif speed == 'DDR':
        return [2, 0]
    elif speed == 'QDR':
        return [4, 0]
    elif speed == 'FDR10':
        return [4, 1]
    elif speed == 'FDR':
        return [4, 1]
    elif speed == 'EDR':
        return [4, 2]
    elif speed == 'HDR':
        return [4, 4]
    elif speed == 'NDR':
        return [4, 8]
    else:
        exit('ERR: unknown/unsupported link speed')


def _conv_lwidth(width):
    if width == 1:
        return 1
    elif width == 4:
        return 2
    elif width == 8:
        return 4
    elif width == 12:
        return 8
    elif width == 2:
        return 16
    else:
        exit('ERR: unknown/unsupported link speed')


def _get_link_width_speed(link, requested_width, requested_speed):
    lwidth, lspeed = 4, 'QDR'

    if isinstance(link, list) and len(link) == 4 and isinstance(link[3], str):
        try:
            lwidth, lspeed = link[3].split('x')
            lwidth = int(lwidth)
        except:
            lwidth, lspeed = 4, 'QDR'
    # overwrite if requested
    if requested_width:
        lwidth = requested_width
    if requested_speed:
        lspeed = requested_speed

    return (lwidth, lspeed)


def _write_ibnetdiscover_file(output_file, topology, supplement_material={},
                              link_width=4, link_speed='QDR',
                              enhanced_sw_port0=False):
    if not isinstance(output_file, str) or not isinstance(topology, dict):
        exit('ERR: invalid function parameter type(s)')

    have_irregular_links = False
    irregular_links = supplement_material.get('irregular_links')
    if irregular_links and isinstance(irregular_links, dict):
        have_irregular_links = True

    with __smart_open(output_file) as out:
        # write switches first (otherwise some tools have problems)
        for sw in sorted(topology.keys(), key=_natural_sort):
            if not topology[sw].get('is_switch'):
                continue
            # put together the switch header
            buffer = 'switchguid=0x%016x\n' % topology[sw]['guid']
            buffer += 'Switch   %i "%s"' % (topology[sw]['radix'], sw)
            if enhanced_sw_port0 or topology[sw].get('esp0'):
                buffer += ' enhanced port 0'
            out.write('%s\n' % buffer)

            buffer = ''
            # write the links connected to this switch
            # sort by egress port number (are int -> already natural order)
            for link in sorted(topology[sw]['adj'], key=lambda link: link[0]):
                lwidth, lspeed = _get_link_width_speed(link,
                                                       link_width,
                                                       link_speed)

                if have_irregular_links:
                    # check if regular or irregular link
                    edge = '%s--%s' % (sw, link[1])
                    rev_edge = '%s--%s' % (link[1], sw)
                    if edge in irregular_links:
                        lwidth, lspeed = irregular_links[edge]
                    elif rev_edge in irregular_links:
                        lwidth, lspeed = irregular_links[rev_edge]

                buffer += '[%i]    "%s"[%i]  %sx%s s=%i w=%i' % (
                    link[0] + 1,
                    link[1],
                    link[2] + 1,
                    lwidth,
                    lspeed,
                    _conv_lspeed(lspeed)[0],
                    _conv_lwidth(lwidth))
                if _conv_lspeed(lspeed)[1] > 0:
                    buffer += ' e=%i\n' % _conv_lspeed(lspeed)[1]
                else:
                    buffer += '\n'
            out.write('%s\n' % buffer)

        # and now write all terminals
        for term in sorted(topology.keys(), key=_natural_sort):
            if topology[term].get('is_switch'):
                continue
            # put together the terminal header
            buffer = 'caguid=0x%016x\n' % topology[term]['guid']
            buffer += 'Hca      %i "%s"' % (topology[term]['radix'], term)
            out.write('%s\n' % buffer)

            buffer = ''
            # write the egress links for the terminal
            for link in sorted(topology[term]['adj'],
                               key=lambda link: link[0]):
                lwidth, lspeed = _get_link_width_speed(link,
                                                       link_width,
                                                       link_speed)

                if have_irregular_links:
                    # check if regular or irregular link
                    edge = '%s--%s' % (term, link[1])
                    rev_edge = '%s--%s' % (link[1], term)
                    if edge in irregular_links:
                        lwidth, lspeed = irregular_links[edge]
                    elif rev_edge in irregular_links:
                        lwidth, lspeed = irregular_links[rev_edge]

                buffer += '[%i]    "%s"[%i]  %sx%s s=%i w=%i' % (
                    link[0] + 1,
                    link[1],
                    link[2] + 1,
                    lwidth,
                    lspeed,
                    _conv_lspeed(lspeed)[0],
                    _conv_lwidth(lwidth))
                if _conv_lspeed(lspeed)[1] > 0:
                    buffer += ' e=%i\n' % _conv_lspeed(lspeed)[1]
                else:
                    buffer += '\n'
            out.write('%s\n' % buffer)


def _write_ibnet_topology(topology, args={}, supplement_material={}):
    if not isinstance(args, dict) or not isinstance(topology, dict):
        exit('ERR: invalid function parameter type(s)')

    output_file = args.get('output_file')
    if output_file is None:
        output_file = ''

    if args.get('__argparse_used__'):
        lwidth, lspeed, esp0 = _get_local_options(args)
    else:
        choice = [1, 2, 4, 8, 12]
        lwidth = args.get('link_width')
        if not lwidth is None and (not isinstance(
                lwidth, int) or choice.count(lwidth) == 0):
            exit('ERR: link_width must be an integer choosen from %s' % choice)

        choice = ['SDR', 'DDR', 'QDR', 'FDR10', 'FDR', 'EDR', 'HDR', 'NDR']
        lspeed = args.get('link_speed')
        if not lspeed is None and (not isinstance(lspeed, str) or choice.count(lspeed) == 0):
            exit('ERR: link_speed must be a string choosen from %s' % choice)

        esp0 = args.get('enhanced_switch_port_0')
        if esp0 is None:
            esp0 = False
        elif not isinstance(esp0, bool):
            exit('ERR: enhanced_switch_port_0 must be of type bool')

    # knowing the port numbers is necessary, and has probably not done before
    if not _topology_is_port_matched(args):
        _topology_port_matching(topology)

    # first write the topology into a ibsim-readable format
    _write_ibnetdiscover_file(output_file,
                              topology,
                              supplement_material,
                              lwidth,
                              lspeed,
                              esp0)

    # and then write supplement material if given
    rootguid_file = ''
    dor_file = ''
    torus2qos_file = ''
    if output_file and output_file != '-':
        dirname, basename = path.split(path.normpath(output_file))
        if not dirname:
            dirname = getcwd()
        basename = basename.split('.')
        basename.insert(0, '')

        basename[0] = 'rootguids'
        rootguid_file = path.join(dirname, '.'.join(basename))

        basename[0] = 'portsearchorderingfile'
        dor_file = path.join(dirname, '.'.join(basename))

        basename[0] = 'torus2qos'
        torus2qos_file = path.join(dirname, '.'.join(basename))

    _write_osm_rootguid_file(rootguid_file, topology)
    if 'osm_dor_input' in supplement_material:
        _write_osm_dor_config_file(
            dor_file,
            topology,
            supplement_material['osm_dor_input'])
    if 'osm_torus2qos_input' in supplement_material:
        _write_osm_torus2qos_config_file(
            torus2qos_file,
            topology,
            supplement_material['osm_torus2qos_input'])


if __name__ == "__main__":
    exit('ERR: not meant to be executed as stand-alone')
