#!/usr/bin/env python3

from sys import exit
from math import ceil, factorial
from itertools import permutations
from argparse import _SubParsersAction
from ..common import _topology_is_valid, _extract_basic_config
from ..common import _get_terminal_name
from ..common import _topology_port_matching
from ..common import _binomial


def _set_local_options(arg_subparser):
    if not isinstance(arg_subparser, _SubParsersAction):
        exit('ERR: needs to be called with existing SubArgumentParser object')

    arg_parser = arg_subparser.add_parser(
        'arrangement',
        description='(n,k)-arrangement graph (see "Arrangement graphs: A class of generalized star graphs")')
    arg_parser.add_argument(
        '--an',
        dest='__arrange_n__',
        type=int,
        help='maximum integer <n> to chose from for the (n,k)-arrangement graph [default: 4]',
        metavar='N',
        default=4)
    arg_parser.add_argument(
        '--ak',
        dest='__arrange_k__',
        type=int,
        help='number of elements <k> in the permutation for the (n,k)-arrangement graph [default: 2]',
        metavar='N',
        default=2)
    arg_parser.add_argument(
        '--ml',
        dest='__arrange_ml__',
        action='store_true',
        help='enable multi-link configuration between switches to fillup empty ports',
        default=False)
    arg_parser.add_argument(
        '--at',
        dest='__arrange_t__',
        type=int,
        help='number of terminals per switch [default: 1]',
        metavar='N',
        default=1)


def _get_local_options(args):
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    n = args.get('__arrange_n__')
    if n < 1:
        exit('ERR: n (given by --an) must be positive')
    k = args.get('__arrange_k__')
    if k < 1 or k >= n:
        exit('ERR: k (given by --kn) must be positive and/or less than n')
    t = args.get('__arrange_t__')
    if t < 1:
        exit('ERR: number of terminals per switch (--at) must be positive')
    multi_link = args.get('__arrange_ml__')

    return (n, k, t, multi_link)


def _get_switch_name(prefix, z):
    return '%s<%s>' % (prefix, ','.join([str(x) for x in z]))


def generate_topology(args={}):
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    topology = {}
    ports = {}

    if args.get('__argparse_used__'):
        n, k, t, multi_link = _get_local_options(args)
        do_port_matching = False
    else:
        do_port_matching = args.get('include_ports')
        if do_port_matching is None:
            do_port_matching = False
        elif not isinstance(do_port_matching, bool):
            exit('ERR: include_ports must be of type bool')

        n = args.get('n')
        if n is None:
            n = 4
        elif not isinstance(n, int) or n < 1:
            exit('ERR: n must be a positive integer')

        k = args.get('k')
        if k is None:
            k = 2
        elif not isinstance(k, int) or k < 1 or k >= n:
            exit('ERR: k must be a positive integer and/or less than n')

        t = args.get('t')
        if t is None:
            t = 1
        elif not isinstance(t, int) or t < 1:
            exit('ERR: t must be a positive integer')

        multi_link = args.get('multi_link')
        if multi_link is None:
            multi_link = False
        elif not isinstance(multi_link, bool):
            exit('ERR: multi_link must be a bool type')

    num_terminals, num_ports_per_terminal, terminal_prefix, \
        num_ports_per_switch, switch_prefix = _extract_basic_config(args)

    num_switches = _binomial(n, k) * factorial(k)

    if num_terminals == -1:
        num_terminals = t * num_switches

    if num_ports_per_switch == -1:
        num_ports_per_switch = t + k * (n - k)

    # generate all switches
    sw_guid, sw_idx = pow(2, 33), 0
    for idx_tuple in permutations(range(1, n + 1), k):
        idx_vector = list(idx_tuple)
        switch = _get_switch_name(switch_prefix, idx_vector)
        topology[switch] = {'guid': sw_guid,
                            'radix': num_ports_per_switch,
                            'adj': [],
                            'is_switch': True}
        ports[switch] = num_ports_per_switch
        # there is no real root/spine for an arrangement graph,
        # so use the first sw if needed
        if sw_idx == 0:
            topology[switch]['fake_root_switch'] = True
        sw_guid -= 26
        sw_idx += 1

    # generate terminals and connect them to switches (round-robin)
    set_term = 0
    while set_term < num_terminals:
        for idx_tuple in permutations(range(1, n + 1), k):
            idx_vector = list(idx_tuple)
            switch = _get_switch_name(switch_prefix, idx_vector)
            terminal = _get_terminal_name(terminal_prefix, set_term)
            topology[terminal] = {
                'guid': 4 * (set_term + 1),
                'radix': num_ports_per_terminal,
                'adj': []}
            for x in range(num_ports_per_terminal):
                topology[switch]['adj'].append(terminal)
                ports[switch] -= 1
            set_term += 1
            if set_term >= num_terminals:
                break

    # connect all switches (multiple times if possible)
    min_ports_needed_for_round = k * (n - k)
    while True:
        for idx_tuple in permutations(range(1, n + 1), k):
            idx_vector_1 = list(idx_tuple)
            switch1 = _get_switch_name(switch_prefix, idx_vector_1)

            for i in range(k):
                p_i = idx_vector_1[i]
                idx_vector_2 = idx_vector_1[:]
                # only consider q_i > p_i, or we'd get two links per edge
                # obviously q_i MUST be valid w.r.t to arrangement graph def
                for q_i in [q_i for q_i in range(
                        p_i + 1, n + 1) if idx_vector_1.count(q_i) == 0]:
                    idx_vector_2[i] = q_i
                    switch2 = _get_switch_name(switch_prefix, idx_vector_2)
                    topology[switch1]['adj'].append(switch2)
                    ports[switch1] -= 1
                    ports[switch2] -= 1

        if not multi_link:
            break
        if min(ports.values()) < min_ports_needed_for_round:
            break

    # is the topology valid?
    if not _topology_is_valid(topology, ports, switch_prefix):
        exit('ERR: not enough ports per switch for all links and terminals')
    elif do_port_matching:
        _topology_port_matching(topology)

    return topology


if __name__ == "__main__":
    exit('ERR: not meant to be executed as stand-alone')
