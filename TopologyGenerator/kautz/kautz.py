#!/usr/bin/env python3

from sys import exit
from copy import deepcopy
from math import ceil
from argparse import _SubParsersAction
from ..common import _topology_is_valid, _extract_basic_config
from ..common import _get_terminal_name
from ..common import _topology_port_matching


def _set_local_options(arg_subparser):
    if not isinstance(arg_subparser, _SubParsersAction):
        exit('ERR: needs to be called with existing SubArgumentParser object')

    arg_parser = arg_subparser.add_parser(
        'kautz',
        description='Undirected topology defined by a Kautz graph (see Network and Parallel Computing NPC 2004, page 309, def. 1-3)')
    arg_parser.add_argument(
        '--kb',
        dest='__kautz_base__',
        type=int,
        help='base (b) of the Kautz string for Kautz graph K(b,n)',
        metavar='N',
        default=2)
    arg_parser.add_argument(
        '--kn',
        dest='__kautz_strlen__',
        type=int,
        help='length (n) of the Kautz string for Kautz graph K(b,n)',
        metavar='N',
        default=3)
    arg_parser.add_argument(
        '--ml',
        dest='__kautz_ml__',
        action='store_true',
        help='enable multi-link configuration between switches to fillup empty ports',
        default=False)


def _get_local_options(args):
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    b = args.get('__kautz_base__')
    if b < 1:
        exit('ERR: Kautz base (--kb) must be positive')
    n = args.get('__kautz_strlen__')
    if n < 1:
        exit('ERR: Kautz string length (--kn) must be positive')
    multi_link = args.get('__kautz_ml__')

    return (b, n, multi_link)


def _get_switch_name(prefix, z):
    return '%s<%s>' % (prefix, ','.join([str(x) for x in z]))


def generate_topology(args={}):
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    topology = {}
    ports = {}

    if args.get('__argparse_used__'):
        b, n, multi_link = _get_local_options(args)
        do_port_matching = False
    else:
        do_port_matching = args.get('include_ports')
        if do_port_matching is None:
            do_port_matching = False
        elif not isinstance(do_port_matching, bool):
            exit('ERR: include_ports must be of type bool')
        b = args.get('base')
        if b is None:
            b = 2
        elif not isinstance(b, int) or b < 1:
            exit('ERR: base must be a positive integer')
        n = args.get('str_len')
        if n is None:
            n = 3
        elif not isinstance(n, int) or n < 1:
            exit('ERR: str_len must be a positive integer')
        multi_link = args.get('multi_link')
        if multi_link is None:
            multi_link = False
        elif not isinstance(multi_link, bool):
            exit('ERR: multi_link must be a bool type')

    num_terminals, num_ports_per_terminal, terminal_prefix, \
        num_ports_per_switch, switch_prefix = _extract_basic_config(args)

    # configure the namespace for Kautz switches
    KautzSpace = []
    z = [0 for x in range(n)]
    for j in range(pow(b + 1, n)):
        for i in range(len(z)):
            z[i] += 1
            if z[i] == b + 1:
                z[i] = 0
            else:
                KautzSpace.append(z[:])
                break
    # test, whether the combination is really a Kautz graph
    # i.e., for all i: z(i) != z(i+1), or remove duplicates
    j = 0
    while j < len(KautzSpace):
        z = KautzSpace[j]
        try:
            for i in range(len(z) - 1):
                if z[i] == z[i + 1]:
                    KautzSpace.remove(z)
                    raise StopIteration()
            j += 1
        except StopIteration:
            pass

    if num_terminals == -1:
        num_terminals = len(KautzSpace)

    if num_ports_per_switch == -1:
        num_ports_per_switch = b * 2 + \
            int(ceil(num_terminals / len(KautzSpace)))

    # generate all switches S<Kautz string>
    sw_guid = pow(2, 33)
    for z in KautzSpace:
        switch = _get_switch_name(switch_prefix, z)
        topology[switch] = {'guid': sw_guid,
                            'radix': num_ports_per_switch,
                            'adj': [],
                            'is_switch': True}
        ports[switch] = num_ports_per_switch
        if KautzSpace.index(z) == 0:
            topology[switch]['fake_root_switch'] = True
        sw_guid -= 26

    # connect terminals
    set_term = 0
    while set_term < num_terminals:
        for z in KautzSpace:
            switch = _get_switch_name(switch_prefix, z)
            terminal = _get_terminal_name(terminal_prefix, set_term)
            topology[terminal] = {
                'guid': 4 * (set_term + 1),
                'radix': num_ports_per_terminal,
                'adj': []}
            for x in range(num_ports_per_terminal):
                topology[switch]['adj'].append(terminal)
                ports[switch] -= 1
            set_term += 1
            if set_term >= num_terminals:
                break

    # connect all switches (multiple times if possible)
    while True:
        # check if we could potentially do another round
        tmp_ports = deepcopy(ports)
        for z in KautzSpace:
            for a in range(b + 1):
                if z[-1] != a:
                    switch1 = _get_switch_name(switch_prefix, z)
                    switch2 = _get_switch_name(switch_prefix, z[1:] + [a])
                    tmp_ports[switch1] -= 1
                    tmp_ports[switch2] -= 1
        # break if some switches don't have enough free ports
        if len([num_free for num_free in list(
                tmp_ports.values()) if num_free < 0]):
            break
        # now actually set a link between switches
        for z in KautzSpace:
            for a in range(b + 1):
                if z[-1] != a:
                    switch1 = _get_switch_name(switch_prefix, z)
                    switch2 = _get_switch_name(switch_prefix, z[1:] + [a])
                    topology[switch1]['adj'].append(switch2)
                    ports[switch1] -= 1
                    ports[switch2] -= 1
        if not multi_link:
            break

    # is the topology valid?
    if not _topology_is_valid(topology, ports, switch_prefix):
        exit('ERR: not enough ports per switch for all links and terminals')
    elif do_port_matching:
        _topology_port_matching(topology)

    return topology


if __name__ == "__main__":
    exit('ERR: not meant to be executed as stand-alone')
