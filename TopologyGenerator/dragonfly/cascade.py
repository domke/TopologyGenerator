#!/usr/bin/env python3

from sys import exit
from argparse import _SubParsersAction
from ..common import _topology_is_valid, _extract_basic_config
from ..common import _get_terminal_name
from ..common import _topology_port_matching


def _set_local_options(arg_subparser):
    if not isinstance(arg_subparser, _SubParsersAction):
        exit('ERR: needs to be called with existing SubArgumentParser object')

    arg_parser = arg_subparser.add_parser(
        'cascade',
        description='Cascade topology (Dragonfly(96,8,10) with flattened butterfly intra-group topology and all-to-all between groups, see "Cray Cascade: a Scalable HPC System based on a Dragonfly Network" for details)')
    arg_parser.add_argument(
        '--cg',
        dest='__cascade_num_groups__',
        type=int,
        help='number of groups (g) and each group is a Dragonfly(a=96,p=8,h=10) [default: 1]',
        metavar='N',
        default=1)
    arg_parser.add_argument(
        '--cl',
        dest='__cascade_num_links__',
        type=int,
        help='number of global links between two groups (min: 4, max: a*h/(g-1)) [default: max]',
        metavar='N',
        default=-1)


def _get_local_options(args):
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    num_groups = args.get('__cascade_num_groups__')
    if num_groups < 1:
        exit('ERR: number of cascade groups (--cg) must be positive')

    num_links = args.get('__cascade_num_links__')
    if num_links < -1:
        exit('ERR: number of links between two groups (--cl) must be positive')

    return (num_groups, num_links)


def _get_switch_name(prefix, group_id, sw_id_x, sw_id_y):
    return '%s<%i,%i,%i>' % (prefix, group_id, sw_id_x, sw_id_y)


def _get_switch_in_group_with_max_fre_ports(
        switch_prefix, switches_per_group, group_id, free_ports):
    max_port_sw = None
    for sw_id_x in range(int(switches_per_group / 6)):
        for sw_id_y in range(int(switches_per_group / 16)):
            switch = _get_switch_name(switch_prefix, group_id, sw_id_x,
                                      sw_id_y)
            if not max_port_sw or free_ports[max_port_sw] < free_ports[switch]:
                max_port_sw = switch

    return max_port_sw


def generate_topology(args={}):
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    topology = {}
    ports = {}

    a, p, h = 96, 8, 10
    if args.get('__argparse_used__'):
        g, b = _get_local_options(args)
        do_port_matching = False
    else:
        do_port_matching = args.get('include_ports')
        if do_port_matching is None:
            do_port_matching = False
        elif not isinstance(do_port_matching, bool):
            exit('ERR: include_ports must be of type bool')

        g = args.get('num_groups')
        if g is None:
            g = 1
        elif not isinstance(g, int) or g < 1:
            exit('ERR: num_groups must be a positive integer')

        b = args.get('num_links')
        if b is None:
            b = -1
        elif not isinstance(b, int) or b < 0:
            exit('ERR: num_links must be a positive integer')

    num_terminals, num_ports_per_terminal, terminal_prefix, \
        num_ports_per_switch, switch_prefix = _extract_basic_config(args)

    if g > (a * h / 4) + 1:
        exit('ERR: invalid config (g > (a*h/4)+1 is impossible)')

    # cascade is defined based on 48-port switches and 8 terminals per switch,
    # so fix the values here
    num_switches = a * g
    num_terminals = num_switches * p
    num_ports_per_switch = 48

    # generate all switches
    sw_guid = pow(2, 33)
    for group_id in range(g):
        # 16 router per chassi
        for sw_id_x in range(int(a / 6)):
            # 6 chassis in one group
            for sw_id_y in range(int(a / 16)):
                switch = _get_switch_name(
                    switch_prefix,
                    group_id,
                    sw_id_x,
                    sw_id_y)
                topology[switch] = {'guid': sw_guid,
                                    'radix': num_ports_per_switch,
                                    'adj': [],
                                    'is_switch': True}
                ports[switch] = num_ports_per_switch
                # there is no real root/spine for a dragonfly
                if group_id == 0 and sw_id_x == 0 and sw_id_y == 0:
                    topology[switch]['fake_root_switch'] = True
                sw_guid -= 26

    # generate terminals and connect them to switches (round-robin)
    set_term = 0
    round = 0
    while set_term < num_terminals or round < p:
        for group_id in range(g):
            for sw_id_x in range(int(a / 6)):
                for sw_id_y in range(int(a / 16)):
                    switch = _get_switch_name(
                        switch_prefix,
                        group_id,
                        sw_id_x,
                        sw_id_y)
                    terminal = _get_terminal_name(terminal_prefix, set_term)
                    topology[terminal] = {
                        'guid': 4 * (set_term + 1),
                        'radix': num_ports_per_terminal,
                        'adj': []}
                    for x in range(num_ports_per_terminal):
                        topology[switch]['adj'].append(terminal)
                        ports[switch] -= 1
                    set_term += 1
                    if set_term >= num_terminals:
                        break
        round += 1

    # connect switches in one group (intra-group connection)
    # each group is a 16x6 mesh w/ all-to-all in each dimension
    for group_id in range(g):
        for src_sw_id_x in range(int(a / 6)):
            for src_sw_id_y in range(int(a / 16)):
                switch1 = _get_switch_name(
                    switch_prefix,
                    group_id,
                    src_sw_id_x,
                    src_sw_id_y)
                # green cables (1x per pair)
                for dst_sw_id_x in range(src_sw_id_x + 1, int(a / 6)):
                    switch2 = _get_switch_name(
                        switch_prefix,
                        group_id,
                        dst_sw_id_x,
                        src_sw_id_y)
                    topology[switch1]['adj'].append(switch2)
                    ports[switch1] -= 1
                    ports[switch2] -= 1
                # black cables (3x per pair)
                for dst_sw_id_y in range(src_sw_id_y + 1, int(a / 16)):
                    switch2 = _get_switch_name(
                        switch_prefix,
                        group_id,
                        src_sw_id_x,
                        dst_sw_id_y)
                    topology[switch1]['adj'].append(switch2)
                    topology[switch1]['adj'].append(switch2)
                    topology[switch1]['adj'].append(switch2)
                    ports[switch1] -= 3
                    ports[switch2] -= 3

    if g > 1:
        max_global_links = int(a * h / (g - 1))
    else:
        max_global_links = 0
    # setup inter-group connections, completely connected (N-to-N)
    for group_id in range(g):
        for remote_group_id in range(group_id + 1, g):
            # 4 links is min. between 2 groups
            for global_link in range(
                    min(max(4, int(b / 4) * 4), max_global_links)):
                # find switch from local group with max. number of free ports
                switch1 = _get_switch_in_group_with_max_fre_ports(
                    switch_prefix, a, group_id, ports)
                # find switch from remote group with max. number of free ports
                switch2 = _get_switch_in_group_with_max_fre_ports(
                    switch_prefix, a, remote_group_id, ports)
                # connect both
                topology[switch1]['adj'].append(switch2)
                ports[switch1] -= 1
                ports[switch2] -= 1

    # is the topology valid?
    if not _topology_is_valid(topology, ports, switch_prefix):
        exit('ERR: not enough ports per switch for all links and terminals')
    elif do_port_matching:
        _topology_port_matching(topology)

    return topology


if __name__ == "__main__":
    exit('ERR: not meant to be executed as stand-alone')
