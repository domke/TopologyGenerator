#!/usr/bin/env python3

from sys import exit
from math import floor
from argparse import _SubParsersAction
from ..common import _topology_is_valid, _extract_basic_config
from ..common import _get_terminal_name
from ..common import _topology_port_matching


def _set_local_options(arg_subparser):
    if not isinstance(arg_subparser, _SubParsersAction):
        exit('ERR: needs to be called with existing SubArgumentParser object')

    arg_parser = arg_subparser.add_parser(
        'dragonfly',
        description='Dragonfly(a,p,h) topology (complete graph used for intra- and inter-group connection, see "Technology-Driven, Highly-Scalable Dragonfly Topology" for details)')
    arg_parser.add_argument(
        '--da',
        dest='__dragonfly_num_routers__',
        type=int,
        help='number of routers/switches (a) in each group [default: 4]',
        metavar='N',
        default=4)
    arg_parser.add_argument(
        '--dp',
        dest='__dragonfly_num_terminals__',
        type=int,
        help='number of terminals (p) connected to each router [default: a/2]',
        metavar='N',
        default=-1)
    arg_parser.add_argument(
        '--dh',
        dest='__dragonfly_num_links__',
        type=int,
        help='number of links within each router to connect to other groups [default: a/2]',
        metavar='N',
        default=-1)
    arg_parser.add_argument(
        '--dg',
        dest='__dragonfly_num_groups__',
        type=int,
        help='number of groups [default: a*h+1 groups]',
        metavar='N',
        default=-1)
    arg_parser.add_argument(
        '--ml',
        dest='__dragonfly_ml__',
        action='store_true',
        help='enable multi-link configuration between switches to fillup empty empty ports',
        default=False)


def _get_local_options(args):
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    num_routers = args.get('__dragonfly_num_routers__')
    if num_routers < 1:
        exit('ERR: number of routers/switches (--da) must be positive')

    num_terminals = args.get('__dragonfly_num_terminals__')
    if num_terminals < -1:
        exit('ERR: number of terminals (--dp) must be positive')

    num_inter_group_links = args.get('__dragonfly_num_links__')
    if num_inter_group_links < -1:
        exit(
            'ERR: number of inter-group links per router (--dh) must be positive')

    num_groups = args.get('__dragonfly_num_groups__')
    if num_groups < -1:
        exit('ERR: number of groups for the dragonfly (--dg) must be positive')

    multi_link = args.get('__dragonfly_ml__')

    return (num_routers, num_terminals, num_inter_group_links, num_groups,
            multi_link)


def _get_switch_name(prefix, group_id, sw_id):
    return '%s<%i,%i>' % (prefix, group_id, sw_id)


def _get_switch_in_group_with_max_fre_ports(
        switch_prefix, switches_per_group, group_id, free_ports):
    max_port_sw = None
    for sw_idx in range(switches_per_group):
        switch = _get_switch_name(switch_prefix, group_id, sw_idx)
        if not max_port_sw or free_ports[max_port_sw] < free_ports[switch]:
            max_port_sw = switch

    return max_port_sw


def generate_topology(args={}):
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    topology = {}
    ports = {}

    if args.get('__argparse_used__'):
        a, p, h, g, multi_link = _get_local_options(args)
        do_port_matching = False
    else:
        do_port_matching = args.get('include_ports')
        if do_port_matching is None:
            do_port_matching = False
        elif not isinstance(do_port_matching, bool):
            exit('ERR: include_ports must be of type bool')

        a = args.get('a')
        if a is None:
            a = 8
        elif not isinstance(a, int) or a < 1:
            exit('ERR: a must be a positive integer')

        p = args.get('p')
        if p is None:
            p = -1
        elif not isinstance(p, int) or p < 0:
            exit('ERR: p must be a positive integer')

        h = args.get('h')
        if h is None:
            h = -1
        elif not isinstance(h, int) or h < 1:
            exit('ERR: h must be a positive integer')

        g = args.get('g')
        if g is None:
            g = -1
        elif not isinstance(g, int) or g < 1:
            exit('ERR: g must be a positive integer')

    num_terminals, num_ports_per_terminal, terminal_prefix, \
        num_ports_per_switch, switch_prefix = _extract_basic_config(args)

    if p == -1:
        p = int(a / 2)
    if h == -1:
        h = int(a / 2)
    if g == -1:
        g = a * h + 1

    if a < 2 * h or p < h:
        exit(
            'ERR: invalid config (valid are: a = 2p = 2h OR (a >= 2h AND 2p >= 2h))')
    if g > a * h + 1:
        exit('ERR: invalid config (g > a*h+1 is impossible)')

    num_switches = a * g

    if num_terminals == -1:
        num_terminals = num_switches * p

    if num_ports_per_switch == -1:
        num_ports_per_switch = a + p + h - 1

    # generate all switches
    sw_guid = pow(2, 33)
    for group_id in range(g):
        for sw_idx in range(a):
            switch = _get_switch_name(switch_prefix, group_id, sw_idx)
            topology[switch] = {'guid': sw_guid,
                                'radix': num_ports_per_switch,
                                'adj': [],
                                'is_switch': True}
            ports[switch] = num_ports_per_switch
            # there is no real root/spine for a dragonfly
            if group_id == 0 and sw_idx == 0:
                topology[switch]['fake_root_switch'] = True
            sw_guid -= 26

    # generate terminals and connect them to switches (round-robin)
    set_term = 0
    conn_round = 0
    while set_term < num_terminals or conn_round < p:
        for group_id in range(g):
            for sw_idx in range(a):
                switch = _get_switch_name(switch_prefix, group_id, sw_idx)
                terminal = _get_terminal_name(terminal_prefix, set_term)
                topology[terminal] = {
                    'guid': 4 * (set_term + 1),
                    'radix': num_ports_per_terminal,
                    'adj': []}
                for x in range(num_ports_per_terminal):
                    topology[switch]['adj'].append(terminal)
                    ports[switch] -= 1
                set_term += 1
                if set_term >= num_terminals:
                    break
        conn_round += 1

    # connect switches in one group (intra-group connection)
    # completely connected (N-to-N)
    while max(ports.values()) >= (a - 1) + floor(int(a * h / (g - 1)) * g / a):
        for group_id in range(g):
            for src_idx in range(a):
                for dst_idx in range(src_idx + 1, a):
                    switch1 = _get_switch_name(switch_prefix, group_id, src_idx)
                    switch2 = _get_switch_name(switch_prefix, group_id, dst_idx)
                    topology[switch1]['adj'].append(switch2)
                    ports[switch1] -= 1
                    ports[switch2] -= 1

        # connect groups with each other (inter-group connection)
        # completely connected (N-to-N)
        for group_id in range(g):
            for remote_group_id in range(group_id + 1, g):
                for global_link in range(int(a * h / (g - 1))):
                    # find switch from local group with max. number of free ports
                    switch1 = _get_switch_in_group_with_max_fre_ports(
                        switch_prefix, a, group_id, ports)
                    # find switch from remote group with max. number of free ports
                    switch2 = _get_switch_in_group_with_max_fre_ports(
                        switch_prefix, a, remote_group_id, ports)
                    # connect both
                    topology[switch1]['adj'].append(switch2)
                    ports[switch1] -= 1
                    ports[switch2] -= 1

        if not multi_link:
            break

    # is the topology valid?
    if not _topology_is_valid(topology, ports, switch_prefix):
        exit('ERR: not enough ports per switch for all links and terminals')
    elif do_port_matching:
        _topology_port_matching(topology)

    return topology


if __name__ == "__main__":
    exit('ERR: not meant to be executed as stand-alone')
