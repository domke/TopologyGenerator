#!/usr/bin/env python3

from sys import exit
from random import seed as seeding


def seed(rng_seed=0):
    """
    seed the RNG internally used
    """
    if not isinstance(rng_seed, int):
        exit('ERR: rng must be seeded with an integer')
    seeding(rng_seed)
