#!/usr/bin/env python3

from sys import exit
from random import randint
from argparse import _SubParsersAction
from ..common import _topology_is_valid, _extract_basic_config
from ..common import _get_terminal_name, _get_switch_name
from ..common import _topology_port_matching


def _set_local_options(arg_subparser):
    if not isinstance(arg_subparser, _SubParsersAction):
        exit('ERR: needs to be called with existing SubArgumentParser object')

    arg_parser = arg_subparser.add_parser(
        'random',
        description='Random topology')
    arg_parser.add_argument(
        '--ns',
        dest='__rand_num_switches__',
        type=int,
        help='number of switches for the random topology [default: 16]',
        metavar='N',
        default=16)
    arg_parser.add_argument(
        '--nl',
        dest='__rand_num_links__',
        type=int,
        help='number of inter-switch links [default: appr. #sw * radix / 4]',
        metavar='N',
        default=-1)


def _get_local_options(args):
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    num_switches = args.get('__rand_num_switches__')
    if num_switches < 1:
        exit('ERR: number of switches (--ns) must be positive')

    num_links = args.get('__rand_num_links__')
    if num_links < -1:
        exit('ERR: number of switch-to-switch links (--nl) must be positive')

    return (num_switches, num_links)


def generate_topology(args={}):
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    topology = {}
    ports = {}

    if args.get('__argparse_used__'):
        num_switches, num_links = _get_local_options(args)
        do_port_matching = False
    else:
        do_port_matching = args.get('include_ports')
        if do_port_matching is None:
            do_port_matching = False
        elif not isinstance(do_port_matching, bool):
            exit('ERR: include_ports must be of type bool')

        num_switches = args.get('num_switches')
        if num_switches is None:
            num_switches = 16
        elif not isinstance(num_switches, int) or num_switches < 0:
            exit('ERR: number of switches must be a positive integer')

        num_links = args.get('num_links')
        if num_links is None:
            num_links = -1
        elif not isinstance(num_links, int) or num_links < -1:
            exit(
                'ERR: number of switch-to-switch links must be a positive integer')

    num_terminals, num_ports_per_terminal, terminal_prefix, \
        num_ports_per_switch, switch_prefix = _extract_basic_config(args)

    if num_terminals == -1:
        num_terminals = num_switches

    if num_ports_per_switch == -1:
        num_ports_per_switch = 16

    if num_links == -1:
        num_links = int(
            (num_switches * num_ports_per_switch - num_terminals * num_ports_per_terminal) / 4)

    # add all switches
    sw_guid = pow(2, 33)
    for sw_id in range(num_switches):
        switch = _get_switch_name(switch_prefix, sw_id)
        topology[switch] = {'guid': sw_guid,
                            'radix': num_ports_per_switch,
                            'adj': [],
                            'is_switch': True}
        ports[switch] = num_ports_per_switch
        if sw_id == 0:
            topology[switch]['fake_root_switch'] = True
        sw_guid -= 26
        # create a switch chain, so that no disjunct topology is possible
        if sw_id > 0:
            switch2 = _get_switch_name(switch_prefix, sw_id - 1)
            topology[switch]['adj'].append(switch2)
            ports[switch] -= 1
            ports[switch2] -= 1
            if num_links > 0:
                num_links -= 1

    # connect terminals to switches (round robin)
    set_term = 0
    while set_term < num_terminals:
        for sw_id in range(num_switches):
            switch = _get_switch_name(switch_prefix, sw_id)
            terminal = _get_terminal_name(terminal_prefix, set_term)
            topology[terminal] = {
                'guid': 4 * (set_term + 1),
                'radix': num_ports_per_terminal,
                'adj': []}
            for x in range(num_ports_per_terminal):
                topology[switch]['adj'].append(terminal)
                ports[switch] -= 1
            set_term += 1
            if set_term >= num_terminals:
                break

    # connect switches randomly
    while num_links > 0:
        switches_w_empty_ports = [sw_id for sw_id in range(num_switches)
                                  if ports[_get_switch_name(switch_prefix, sw_id)] > 0]
        if len(switches_w_empty_ports) < 2:
            break
        rand_idx_1 = randint(0, len(switches_w_empty_ports) - 1)
        rand_idx_2 = randint(0, len(switches_w_empty_ports) - 1)
        while rand_idx_1 == rand_idx_2:
            rand_idx_2 = randint(0, len(switches_w_empty_ports) - 1)
        switch1 = _get_switch_name(
            switch_prefix,
            switches_w_empty_ports[rand_idx_1])
        switch2 = _get_switch_name(
            switch_prefix,
            switches_w_empty_ports[rand_idx_2])
        topology[switch1]['adj'].append(switch2)
        ports[switch1] -= 1
        ports[switch2] -= 1
        num_links -= 1

    # is the topology valid?
    if num_links > 0 or not _topology_is_valid(topology, ports, switch_prefix):
        exit('ERR: not enough ports per switch for all links and terminals')
    elif do_port_matching:
        _topology_port_matching(topology)

    return topology


if __name__ == "__main__":
    exit('ERR: not meant to be executed as stand-alone')
