#!/usr/bin/env python3

from sys import exit
from argparse import ArgumentParser
from re import split
from .rand.rng import seed
from functools import reduce


def _get_arg_parser():
    return ArgumentParser()


def _set_global_options(arg_parser):
    if not arg_parser or not isinstance(arg_parser, ArgumentParser):
        exit('ERR: needs to be called with existing ArgumentParser object')

    arg_parser.add_argument(
        '-o',
        dest='output_file',
        help='name of the output file [default: stdout]',
        metavar='<output file>',
        default='')
    arg_parser.add_argument(
        '--ot',
        dest='output_type',
        help='format of the output [default: ibnet]',
        choices=['dot', 'ibnet'],
        default='ibnet')
    arg_parser.add_argument(
        '--nt',
        dest='num_terminals',
        type=int,
        help='number of terminal nodes; distributed in round-robin fashion [default: 1 per (leaf-)switch]',
        metavar='N',
        default=None)
    arg_parser.add_argument(
        '--ntp',
        dest='num_ports_per_terminal',
        type=int,
        help='number of terminal ports [default: 1]',
        choices=[1, 2, 4],
        default=1)
    arg_parser.add_argument(
        '--nsp',
        dest='num_ports_per_switch',
        type=int,
        help='number of ports per switch',
        metavar='N',
        default=None)
    arg_parser.add_argument(
        '--pt',
        dest='terminal_prefix',
        help='prefix for terminals in output [default: "T"]',
        metavar='<prefix>',
        default='T')
    arg_parser.add_argument(
        '--ps',
        dest='switch_prefix',
        help='prefix for switches [default: "S"]',
        metavar='<prefix>',
        default='S')
    arg_parser.add_argument(
        '--seed',
        dest='rng_seed',
        type=int,
        help='seed for the RNG if repeatability is required',
        metavar='N',
        default=None)
    arg_parser.add_argument(
        '--verbose',
        dest='verbose',
        action='store_true',
        help='provide summery for the created topology (#switches, #terminals, etc.)',
        default=False)


def _topology_is_valid(topology, ports, sw_pref='S'):
    if not topology or not ports:
        return False
    if not isinstance(topology, dict) or not isinstance(ports, dict):
        exit('ERR: invalid function parameter type(s)')
    if not isinstance(sw_pref, str):
        exit('ERR: invalid function parameter type(s)')

    for switch in topology:
        if switch.find(sw_pref) == 0:
            if ports[switch] < 0:
                return False

    return True


def _extract_basic_config(args):
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    num_terminals = args.get('num_terminals')
    terminal_prefix = args.get('terminal_prefix')
    switch_prefix = args.get('switch_prefix')
    num_ports_per_terminal = args.get('num_ports_per_terminal')
    num_ports_per_switch = args.get('num_ports_per_switch')

    if num_terminals is None:
        num_terminals = -1
    elif not isinstance(num_terminals, int) or num_terminals < 0:
        exit('ERR: num_terminals must be positive and of type integer')

    if 'terminal_prefix' not in args:
        terminal_prefix = 'T'
    elif not isinstance(terminal_prefix, str):
        exit('ERR: terminal_prefix must be of type str')

    if 'switch_prefix' not in args:
        switch_prefix = 'S'
    elif not isinstance(switch_prefix, str):
        exit('ERR: switch_prefix must be of type str')

    if num_ports_per_terminal is None:
        num_ports_per_terminal = 1
    elif not isinstance(num_ports_per_terminal, int) or num_ports_per_terminal < 1:
        exit(
            'ERR: num_ports_per_terminal must be positive and of type integer')

    if num_ports_per_switch is None:
        num_ports_per_switch = -1
    elif not isinstance(num_ports_per_switch, int) or num_ports_per_switch < 0:
        exit('ERR: num_ports_per_switch must be positive and of type integer')

    return (num_terminals, num_ports_per_terminal, terminal_prefix,
            num_ports_per_switch, switch_prefix)


def _get_terminal_name(prefix, id):
    return '%s<%i>' % (prefix, id)


def _get_switch_name(prefix, id):
    return '%s<%i>' % (prefix, id)


def _init_rng(args):
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    rng_seed = args.get('rng_seed')
    if rng_seed:
        seed(rng_seed)


def _binomial(n, k):
    if k < 0 or k > n:
        return 0
    elif k == 0 or n == k:
        return 1
    elif k == 1:
        return n
    v = [z for z in range(n + 1)]
    return int(reduce(lambda x, y: x * y,
                      v[n - k + 1:]) / reduce(lambda x, y: x * y, v[1:k + 1]))


def _atoi(str):
    return int(str) if str.isdigit() else str


def _natural_key(str):
    return [_atoi(c) for c in split(r'(\d+)', str)]


def _natural_sort(item):
    if isinstance(item, str):
        node_name = item
    elif isinstance(item, list) and len(item) == 3:
        node_name = item[1]
    else:
        exit('ERR: unknown/unsupported format in _natural_sort')
    return _natural_key(node_name)


def _get_next_free_port(node, topology, port_map):
    if not isinstance(node, str) or not isinstance(
            topology, dict) or not isinstance(port_map, dict):
        exit('ERR: invalid function parameter type(s)')

    ports_in_use = [link[0] for link in port_map[node]] + \
        [link[0] for link in topology[node]['adj']
         if isinstance(link, list) and len(link) == 3]
    ports_in_use = sorted(set(ports_in_use))

    # if the node has no/one ports in use, then start with port number 0
    if len(ports_in_use) == 0:
        return 0
    elif len(ports_in_use) == 1:
        if ports_in_use[0] == 0:
            return 1
        else:
            return 0

    # check if there is a gap in between the already used ports
    for i in range(len(ports_in_use) - 1):
        if ports_in_use[i] + 1 < ports_in_use[i + 1]:
            free_port = ports_in_use[i] + 1
            break
    else:
        # or take next higher port number
        free_port = ports_in_use[-1] + 1

    return free_port


def _topology_port_matching(topology):
    if not isinstance(topology, dict):
        exit('ERR: invalid function parameter type(s)')

    # premiss for this function:
    # a) asymmetrical topology -> links are stored only at one end point
    # b) links can be either of the three forms:
    #    1) [<local port> <remote node name> <remote port>]
    #    2) [<local port> <remote node name> -1]
    #    3) <remote node name>

    # create a local port_map
    port_map = {}
    for node in topology:
        port_map[node] = []

    # do the port matching in three stages, from most we know to least we know
    # first stage: we have full knowledge (local and remote port)
    for node in sorted(topology.keys(), key=_natural_sort):
        for link in sorted(topology[node]['adj'], key=_natural_sort):
            # only move on if the link is a 3-element list
            if not isinstance(link, list) or len(link) != 3:
                continue

            # unpack the link and check if we know the remote port
            lport, remote_node, rport = link
            if not isinstance(lport, int) or not isinstance(
                    remote_node, str) or not isinstance(rport, int):
                exit('ERR: unknown/unsupported link format')
            if rport < 0:
                continue

            # sanity check: if port matching has been performed before -> error
            if topology[remote_node]['adj'].count([rport, node, lport]) > 0:
                exit(
                    'ERR: port matching not allowed for already matched topology')

            # add the link to local and remote node if not already included
            if port_map[node].count([lport, remote_node, rport]) == 0:
                port_map[node].append([lport, remote_node, rport])
            if port_map[remote_node].count([rport, node, lport]) == 0:
                port_map[remote_node].append([rport, node, lport])

    # second stage: partial knowledge (local port known, but not remote port)
    for node in sorted(topology.keys(), key=_natural_sort):
        for link in sorted(topology[node]['adj'], key=_natural_sort):
            # only move on if the link is a 3-element list
            if not isinstance(link, list) or len(link) != 3:
                continue

            # unpack the link and check if remote port is unknown
            # (validity checked before)
            lport, remote_node, rport = link
            if rport > -1:
                continue

            rport = _get_next_free_port(remote_node, topology, port_map)

            # add the link to local and remote node if not already included
            if port_map[node].count([lport, remote_node, rport]) == 0:
                port_map[node].append([lport, remote_node, rport])
            if port_map[remote_node].count([rport, node, lport]) == 0:
                port_map[remote_node].append([rport, node, lport])

    # and last/third stage were we only know the remote name
    for node in sorted(topology.keys(), key=_natural_sort):
        for link in sorted(topology[node]['adj'], key=_natural_sort):
            # only move on with pure node names in adjacency list
            if not isinstance(link, str):
                continue

            lport = _get_next_free_port(node, topology, port_map)

            remote_node = link
            rport = _get_next_free_port(remote_node, topology, port_map)

            # add the link to local and remote node if not already included
            if port_map[node].count([lport, remote_node, rport]) == 0:
                port_map[node].append([lport, remote_node, rport])
            if port_map[remote_node].count([rport, node, lport]) == 0:
                port_map[remote_node].append([rport, node, lport])

    # write back the port mapping into the topology
    for node in port_map:
        topology[node]['adj'] = port_map[node][:]


def _find_all_fully_known_in_local_list(loc_links, rem_links):
    if not isinstance(loc_links, list) or not isinstance(rem_links, list):
        exit('ERR: invalid function parameter type(s)')

    # sort so that known -> partially known (1x -1) -> nothing (2x -1)
    loc_links.sort(reverse=True)
    rem_links.sort(reverse=True)

    # after this we have have all full known links
    for i in range(len(loc_links)):
        loc_link = loc_links[i]
        # find matching pairs
        for j in range(len(rem_links)):
            rem_link = rem_links[j]
            # full knowledge locally
            if loc_link[0] > -1 and loc_link[2] > -1:
                if loc_link[2] == rem_link[0]:
                    # found a match
                    rem_links[j][2] = loc_link[0]
                    break
                elif rem_link[0] == -1:
                    # found no match -> take first with (2x -1)
                    rem_links[j][0] = loc_link[2]
                    rem_links[j][2] = loc_link[0]
                    break
            # parital knowledge locally
            elif loc_link[0] > -1:
                # and full knowledge on the remote side
                if rem_link[2] == loc_link[0]:
                    loc_links[i][2] = rem_link[0]
                    break
                # or first one with partial knowledge -> match these two
                elif rem_link[0] > -1 and rem_link[2] == -1:
                    loc_links[i][2] = rem_link[0]
                    rem_links[j][2] = loc_link[0]
                    break


def _topology_port_matching_symmetrical(topology):
    if not isinstance(topology, dict):
        exit('ERR: invalid function parameter type(s)')

    # premiss for this function:
    # a) symmetrical input topology -> links are stored on both end points
    # b) links can be either of the three forms:
    #    1) [<local port> <remote node name> <remote port>]
    #    2) [<local port> <remote node name> -1]
    #    3) <remote node name>

    # create a local port_map
    port_map = {}
    for node in topology:
        port_map[node] = []

    # assemble a list of nodes to have a ordering thru the index in the list
    node_list = [sw for sw in topology if topology[sw].get('is_switch')] + \
        [term for term in topology if not topology[term].get('is_switch')]

    for loc_node in topology:
        node_idx = node_list.index(loc_node)

        # get the set of neighbor nodes with higher index than loc_node
        adj_list = [link[1] for link in topology[loc_node]['adj']
                    if isinstance(link, list)
                    and len(link) == 3] + \
                   [link for link in topology[loc_node]['adj']
                    if isinstance(link, str)]
        adj_list = list(set(adj_list))
        adj_list = [adj for adj in adj_list if node_list.index(adj) > node_idx]

        for rem_node in adj_list:
            # get all links between local node and remote node (and reverse)
            loc_links = [link for link in topology[loc_node]['adj']
                         if isinstance(link, list)
                         and link[1] == rem_node] + \
                        [[-1, link, -1] for link in topology[loc_node]['adj']
                         if isinstance(link, str) and link == rem_node]
            rem_links = [link for link in topology[rem_node]['adj']
                         if isinstance(link, list)
                         and link[1] == loc_node] + \
                        [[-1, link, -1] for link in topology[rem_node]['adj']
                         if isinstance(link, str) and link == rem_node]

            if len(loc_links) != len(rem_links):
                exit(
                    'ERR: incompatible DOT format, mismatch in number of links between node %s and node %s' %
                    (loc_node, rem_node))

            # first get the maximum set of fully known links of both lists
            _find_all_fully_known_in_local_list(loc_links, rem_links)
            _find_all_fully_known_in_local_list(rem_links, loc_links)

            # add the link to local and remote node if not already included
            for link in [link for link in loc_links if link[2] > -1]:
                if port_map[loc_node].count(link) == 0:
                    port_map[loc_node].append(link)
            for link in [link for link in rem_links if link[2] > -1]:
                if port_map[rem_node].count(link) == 0:
                    port_map[rem_node].append(link)

            # sort so that known -> partially known (1x -1) -> nothing (2x -1)
            loc_links.sort(reverse=True)
            rem_links.sort(reverse=True)

            # now fix the partially/unknown links with yet unknown reverse link
            for link in [link for link in loc_links if link[2] == -1]:
                if link[0] == -1:
                    lport = _get_next_free_port(loc_node, topology, port_map)
                else:
                    lport = link[0]
                rport = _get_next_free_port(rem_node, topology, port_map)
                port_map[loc_node].append([lport, rem_node, rport])
                port_map[rem_node].append([rport, loc_node, lport])
            for link in [link for link in rem_links if link[2] == -1]:
                if link[0] == -1:
                    lport = _get_next_free_port(rem_node, topology, port_map)
                else:
                    lport = link[0]
                rport = _get_next_free_port(loc_node, topology, port_map)
                port_map[rem_node].append([lport, loc_node, rport])
                port_map[loc_node].append([rport, rem_node, lport])

    # sanity check if we really fix all links and merge back into topology
    for node in port_map:
        for lport, remote_node, rport in port_map[node]:
            if -1 == lport or -1 == rport:
                exit('ERR: could not fix all unknown ports')
        topology[node]['adj'] = port_map[node][:]


def _topology_remove_port_matching(topology):
    if not isinstance(topology, dict):
        exit('ERR: invalid function parameter type(s)')

    node_list = [sw for sw in topology if topology[sw].get('is_switch')] + \
        [term for term in topology if not topology[term].get('is_switch')]

    for node in topology:
        node_idx = node_list.index(node)
        adj_list = [link[1]
                    for link in topology[node]['adj']
                    if node_list.index(link[1]) > node_idx]
        topology[node]['adj'] = adj_list[:]


def _topology_is_port_matched(args={}):
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    if args.get('include_ports'):
        return True
    elif args.get('__argparse_used__') and args.get('__load_input_file__'):
        return True

    return False


if __name__ == '__main__':
    exit('ERR: not meant to be executed as stand-alone')
