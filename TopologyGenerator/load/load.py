#!/usr/bin/env python3

from sys import exit
from os import path, getcwd
from argparse import _SubParsersAction
from ..io.io import _read


def _set_local_options(arg_subparser):
    if not isinstance(arg_subparser, _SubParsersAction):
        exit('ERR: needs to be called with existing SubArgumentParser object')

    arg_parser = arg_subparser.add_parser(
        'load',
        description='Load an existing topology and modify/convert it')
    arg_parser.add_argument(
        '-i',
        dest='__load_input_file__',
        help='name of existing DOT/topology file to load and modify',
        required=True,
        metavar='<input file>',
        default=None)
    arg_parser.add_argument(
        '--it',
        dest='__load_input_type__',
        help='format of the input file [default: ibnet]',
        choices=['dot', 'ibnet'],
        default='ibnet')
    arg_parser.add_argument(
        '--irg',
        dest='__load_input_rootguid_file__',
        help='name of an existing rootguid file for an ibnet-based topology',
        metavar='<input file>',
        default=None)


def _get_local_options(args):
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    input_file = args.get('__load_input_file__')
    input_type = args.get('__load_input_type__')
    input_rootguid_file = args.get('__load_input_rootguid_file__')

    return (input_file, input_type, input_rootguid_file)


def load(args={}):
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    topology = {}

    if args.get('__argparse_used__'):
        in_file, in_type, in_rguid = _get_local_options(args)
    else:
        in_file = args.get('input_file')
        if in_file is None or not isinstance(in_file, str):
            exit('ERR: input_file string required in provided dict')

        in_type = args.get('input_type')
        if in_type is None:
            in_type = 'ibnet'
        elif not isinstance(in_type, str) or ['dot', 'ibnet'].count(in_type) == 0:
            exit('ERR: input_type string must be choosen from {dot, ibnet}')

        in_rguid = args.get('input_rootguid_file')
        if in_rguid is not None and not isinstance(in_rguid, str):
            exit('ERR: input_rootguid_file must be of type string')

    dirname, basename = path.split(path.normpath(in_file))
    if not dirname:
        dirname = getcwd()
    in_file = path.join(dirname, basename)
    if not path.exists(in_file):
        exit('ERR: input file (%s) not found' % in_file)

    if in_rguid:
        dirname, basename = path.split(path.normpath(in_rguid))
        if not dirname:
            dirname = getcwd()
        in_rguid = path.join(dirname, basename)
        if not path.exists(in_rguid):
            exit('ERR: input file (%s) not found' % in_rguid)

    _read(topology, in_file, in_type, in_rguid, args)

    return topology


if __name__ == "__main__":
    exit('ERR: not meant to be executed as stand-alone')
