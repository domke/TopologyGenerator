#!/usr/bin/env python3

from sys import exit
from math import ceil, floor
from argparse import _SubParsersAction
from functools import reduce
from ..common import _topology_is_valid, _extract_basic_config
from ..common import _get_terminal_name
from ..common import _topology_port_matching


def _set_local_options(arg_subparser):
    if not isinstance(arg_subparser, _SubParsersAction):
        exit('ERR: needs to be called with existing SubArgumentParser object')

    arg_parser = arg_subparser.add_parser(
        'xgft',
        description='eXtended Generalized Fat Tree topology (see "On generalized fat trees" for details')
    arg_parser.add_argument(
        '--xh',
        dest='__xgft_tree_height__',
        type=int,
        help='height of the tree (number of levels + 1) [default: 2 (-> 3 level tree)]',
        metavar='N',
        default=2)
    arg_parser.add_argument(
        '--xm',
        dest='__xgft_children_per_level__',
        type=int,
        nargs='+',
        help='number of child nodes per level (0 < i <= h; L0 are the leafs) [default: 4 4]',
        metavar='N',
        default=[4, 4])
    arg_parser.add_argument(
        '--xw',
        dest='__xgft_parents_per_level__',
        type=int,
        nargs='+',
        help='number of parent nodes per level (0 <= i < h) [default: 2 2]',
        metavar='N',
        default=[2, 2])
    arg_parser.add_argument(
        '--ml',
        dest='__xgft_ml__',
        action='store_true',
        help='enable multi-link configuration between switches to fillup empty ports',
        default=False)


def _get_local_options(args):
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    h = args.get('__xgft_tree_height__')
    if h < 0:
        exit('ERR: tree height (--xh) must be positive')

    m = args.get('__xgft_children_per_level__')
    if min(m) < 1 or len(m) != h:
        exit(
            'ERR: number of child nodes must be a list of positive integers of \'tree height\' length')

    w = args.get('__xgft_parents_per_level__')
    if min(w) < 1 or len(w) != h:
        exit(
            'ERR: number of parent nodes must be a list of positive integers of \'treeheight\' length')

    multi_link = args.get('__xgft_ml__')

    return (h, m, w, multi_link)


def _get_switch_name(prefix, id_vector):
    return '%s<%s>' % (prefix, ','.join([str(x) for x in id_vector]))


def _get_switch_level(name_str, id_vector):
    if name_str is not None and name_str != '':
        return int(name_str.split(",")[0].split("<")[1])
    elif len(id_vector) > 0:
        return id_vector[0]
    else:
        return None


def _get_array_product(array):
    if len(array) == 0:
        return 1
    else:
        return reduce(lambda x, y: x * y, array)


def _get_lambda(m, w, lvl, h):
    _lambda = _get_array_product(w[1:lvl + 1])
    _lambda *= _get_array_product(m[lvl + 1:h + 1])

    return _lambda


def _get_Vjh(j, h, m, w):
    Vjh = []

    for lvl in range(0, h + 1):
        lam = _get_lambda(m, w, lvl, h)
        for a in range(j * lam, (j + 1) * lam):
            Vjh.append([lvl, a])

    return Vjh


def _get_Vh(hp1, m, w):
    if not isinstance(hp1, int) or not isinstance(
            m, list) or not isinstance(w, list):
        exit('ERR: invalid function parameter type(s)')

    if hp1 == 0:
        return [[0, 0]]

    # first part
    Vjh = []
    h = hp1 - 1
    for j in range(0, m[hp1]):
        Vjh.extend(_get_Vjh(j, h, m, w))
    # second part
    Vh = []
    for a in range(0, _get_array_product(w[1:hp1 + 1])):
        Vh.append([hp1, a])
    # add two sets and return
    Vjh.extend(Vh)

    return Vjh


def _get_Ejh(j, h, m, w):
    Ejh = []
    Vjh = _get_Vjh(j, h, m, w)
    Eh = _get_Eh(h, m, w)

    for lvl in range(0, h):
        lam1 = _get_lambda(m, w, lvl, h)
        lam2 = _get_lambda(m, w, lvl + 1, h)
        wh = _get_array_product(w[1:h + 1])
        for la in Vjh:
            if la[0] != lvl:
                continue
            for lb in Vjh:
                if lb[0] != lvl + 1:
                    continue
                if Eh.count([[la[0], la[1] - j * lam1],
                             [lb[0], lb[1] - j * lam2]]) > 0:
                    Ejh.append([la, lb])

    return Ejh


def _get_Eh(hp1, m, w):
    if not isinstance(hp1, int) or not isinstance(
            m, list) or not isinstance(w, list):
        exit('ERR: invalid function parameter type(s)')

    if hp1 == 0:
        return []

    Eh = []
    h = hp1 - 1

    # first part
    for j in range(0, m[hp1]):
        Eh.extend(_get_Ejh(j, h, m, w))
    # second part
    lam1 = _get_lambda(m, w, h, hp1)
    lam2 = _get_lambda(m, w, hp1, hp1)
    wh = _get_array_product(w[1:h + 1])
    for b in range(0, lam2):
        for a in range(0, lam1):
            if a % wh == int(b / w[hp1]):
                Eh.append([[h, a], [hp1, b]])

    return Eh


def generate_topology(args={}):
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    topology = {}
    ports = {}

    if args.get('__argparse_used__'):
        h, m, w, multi_link = _get_local_options(args)
        do_port_matching = False
    else:
        do_port_matching = args.get('include_ports')
        if do_port_matching is None:
            do_port_matching = False
        elif not isinstance(do_port_matching, bool):
            exit('ERR: include_ports must be of type bool')

        h = args.get('tree_height')
        if h is None:
            h = 2
        elif not isinstance(h, int) or h < 0:
            exit('ERR: h must be positive integer')

        m = args.get('num_children_per_level')
        if m is None:
            m = [4, 4]
        elif not all(isinstance(x, int) for x in m) or min(m) < 1 or len(m) != h:
            exit(
                'ERR: num_children_per_level must be a list of positive integers of \'tree_height\' length')

        w = args.get('num_parents_per_level')
        if w is None:
            w = [2, 2]
        elif not all(isinstance(x, int) for x in w) or min(w) < 1 or len(w) != h:
            exit(
                'ERR: num_parents_per_level must be a list of positive integers of \'tree_height\' length')

    num_terminals, num_ports_per_terminal, terminal_prefix, \
        num_ports_per_switch, switch_prefix = _extract_basic_config(args)

    # add dummy at beginning
    m.insert(0, 0)
    w.insert(0, 0)

    # generate a list of all switch nodes (Vh) and list of edges (Eh)
    Vh = _get_Vh(h, m, w)
    Eh = _get_Eh(h, m, w)

    num_switches = len(Vh)
    num_leaf_switches = 0
    for xgft_str in Vh:
        sw_lvl = _get_switch_level(None, xgft_str)
        if sw_lvl == 0:
            num_leaf_switches += 1

    if num_terminals == -1:
        num_terminals = num_leaf_switches

    if num_ports_per_switch == -1:
        min_ports_needed = ceil(num_terminals / num_leaf_switches) \
            * num_ports_per_terminal + w[1]
        for lvl in range(1, h):
            min_ports_needed = max(m[lvl] + w[lvl + 1], min_ports_needed)
        num_ports_per_switch = min_ports_needed

    # generate all switch names and add them to the topology
    root_sw_guid = pow(2, 33)
    non_root_sw_guid = pow(2, 33) - 26
    for xgft_str in Vh:
        switch = _get_switch_name(switch_prefix, xgft_str)
        topology[switch] = {'guid': None,
                            'radix': num_ports_per_switch,
                            'adj': [],
                            'is_switch': True}
        ports[switch] = num_ports_per_switch
        sw_lvl = _get_switch_level(None, xgft_str)
        if h - sw_lvl == 0:
            topology[switch]['is_root'] = True
            topology[switch]['guid'] = root_sw_guid
            root_sw_guid += 26
        else:
            topology[switch]['guid'] = non_root_sw_guid
            non_root_sw_guid -= 26

    # connect all switches
    if multi_link:
        t_ports = ceil(num_terminals / num_leaf_switches) \
            * num_ports_per_terminal
        num_multi_link = max(1,
                             floor((num_ports_per_switch - max(t_ports, w[1]))
                                   / w[1]))
        min_ports_needed = 0
        for lvl in range(1, h):
            min_ports_needed = max(m[lvl] + w[lvl + 1], min_ports_needed)
        num_multi_link = min(num_multi_link,
                             floor(num_ports_per_switch / min_ports_needed))
    else:
        num_multi_link = 1
    for xgft_str_1, xgft_str_2 in Eh:
        switch1 = _get_switch_name(switch_prefix, xgft_str_1)
        switch2 = _get_switch_name(switch_prefix, xgft_str_2)
        topology[switch1]['adj'] += num_multi_link*[switch2]
        ports[switch1] -= num_multi_link
        ports[switch2] -= num_multi_link

    # connect terminals
    set_term = 0
    while set_term < num_terminals:
        for xgft_str in Vh:
            sw_lvl = _get_switch_level(None, xgft_str)
            if sw_lvl != 0:
                continue
            switch = _get_switch_name(switch_prefix, xgft_str)
            terminal = _get_terminal_name(terminal_prefix, set_term)
            topology[terminal] = {
                'guid': 4 * (set_term + 1),
                'radix': num_ports_per_terminal,
                'adj': []}
            for x in range(num_ports_per_terminal):
                topology[switch]['adj'].append(terminal)
                ports[switch] -= 1
            set_term += 1
            if set_term >= num_terminals:
                break

    # is the topology valid?
    if not _topology_is_valid(topology, ports, switch_prefix):
        exit('ERR: not enough ports per switch for all links and terminals')
    elif do_port_matching:
        _topology_port_matching(topology)

    return topology


if __name__ == "__main__":
    exit('ERR: not meant to be executed as stand-alone')
