#!/usr/bin/env python3

from sys import exit
from argparse import _SubParsersAction
from ..common import _topology_is_valid, _extract_basic_config
from ..common import _get_terminal_name
from ..common import _topology_port_matching


def _set_local_options(arg_subparser):
    if not isinstance(arg_subparser, _SubParsersAction):
        exit('ERR: needs to be called with existing SubArgumentParser object')

    arg_parser = arg_subparser.add_parser(
        'fat-tree',
        description='Fat-Tree topology (binary tree with more switch-to-switch links towards the root)')
    arg_parser.add_argument(
        '--ftl',
        dest='__fat_tree_levels__',
        type=int,
        help='number of levels of the binay tree [default: 3]',
        metavar='N',
        default=3)


def _get_local_options(args):
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    lvl = args.get('__fat_tree_levels__')
    if lvl < 1:
        exit('ERR: number of levels (--ftl) must be positive')

    return lvl


def _get_switch_name(prefix, lvl, sw_in_lvl):
    return '%s<%i,%i>' % (prefix, lvl, sw_in_lvl)


def generate_topology(args={}):
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    topology = {}
    ports = {}

    if args.get('__argparse_used__'):
        ft_lvl = _get_local_options(args)
        do_port_matching = False
    else:
        do_port_matching = args.get('include_ports')
        if do_port_matching is None:
            do_port_matching = False
        elif not isinstance(do_port_matching, bool):
            exit('ERR: include_ports must be of type bool')

        ft_lvl = args.get('levels')
        if ft_lvl is None:
            ft_lvl = 3
        elif not isinstance(ft_lvl, int) or ft_lvl < 1:
            exit('ERR: level must be a positive integer')

    num_terminals, num_ports_per_terminal, terminal_prefix, \
        num_ports_per_switch, switch_prefix = _extract_basic_config(args)

    num_switches = pow(2, ft_lvl) - 1

    if num_terminals == -1:
        num_terminals = pow(2, ft_lvl - 1)

    if num_ports_per_switch == -1:
        num_ports_per_switch = pow(2, ft_lvl - 1)

    # create fat-tree switches (binary tree)
    sw_guid = pow(2, 33)
    for lvl in range(ft_lvl):
        for sw_in_lvl in range(pow(2, lvl)):
            switch = _get_switch_name(switch_prefix, lvl, sw_in_lvl)
            topology[switch] = {'guid': sw_guid,
                                'radix': num_ports_per_switch,
                                'adj': [],
                                'is_switch': True}
            ports[switch] = num_ports_per_switch
            if lvl == 0:
                topology[switch]['is_root'] = True
            sw_guid -= 26

    # connect switches
    for lvl in range(ft_lvl - 1):
        for parent_id in range(pow(2, lvl)):
            parent_sw = _get_switch_name(switch_prefix, lvl, parent_id)
            child_sw1 = _get_switch_name(switch_prefix, lvl + 1, parent_id * 2)
            child_sw2 = _get_switch_name(
                switch_prefix,
                lvl + 1,
                parent_id * 2 + 1)
            # first half of ports for child1, second half for child2
            for port in range(pow(2, ft_lvl - lvl - 2)):
                topology[parent_sw]['adj'].append(child_sw1)
                ports[parent_sw] -= 1
                ports[child_sw1] -= 1
                topology[parent_sw]['adj'].append(child_sw2)
                ports[parent_sw] -= 1
                ports[child_sw2] -= 1

    # connect terminals (leaves of the tree)
    set_term = 0
    while set_term < num_terminals:
        for parent_id in range(pow(2, ft_lvl - 1)):
            parent_sw = _get_switch_name(switch_prefix, ft_lvl - 1, parent_id)
            terminal = _get_terminal_name(terminal_prefix, set_term)
            topology[terminal] = {
                'guid': 4 * (set_term + 1),
                'radix': num_ports_per_terminal,
                'adj': []}
            for x in range(num_ports_per_terminal):
                topology[parent_sw]['adj'].append(terminal)
                ports[parent_sw] -= 1
            set_term += 1
            if set_term >= num_terminals:
                break

    # is the topology valid?
    if not _topology_is_valid(topology, ports, switch_prefix):
        exit('ERR: not enough ports per switch for all links and terminals')
    elif do_port_matching:
        _topology_port_matching(topology)

    return topology


if __name__ == "__main__":
    exit('ERR: not meant to be executed as stand-alone')
