#!/usr/bin/env python3

from sys import exit
from math import ceil, floor
from argparse import _SubParsersAction
from ..common import _topology_is_valid, _extract_basic_config
from ..common import _get_terminal_name
from ..common import _topology_port_matching


def _set_local_options(arg_subparser):
    if not isinstance(arg_subparser, _SubParsersAction):
        exit('ERR: needs to be called with existing SubArgumentParser object')

    arg_parser = arg_subparser.add_parser(
        'k-ary-n',
        description='k-ary n-tree topology (see "k-ary n-trees: High Performance Networks for Massively Parallel Architectures", page 3, def. 2.2 for details)')
    arg_parser.add_argument(
        '--tk',
        dest='__karyn_half_radix__',
        type=int,
        help='k is half the number of ports for each switch [default: 4]',
        metavar='N',
        default=4)
    arg_parser.add_argument(
        '--tn',
        dest='__karyn_lvls__',
        type=int,
        help='n is the number of levels in the tree [default: 2]',
        metavar='N',
        default=2)
    arg_parser.add_argument(
        '--tsp',
        dest='__karyn_spine_pruned__',
        action='store_true',
        help='spine pruned k-ary n-tree (half the number of spine switches to eliminate unused ports in the spines)',
        default=False)
    arg_parser.add_argument(
        '--tpp',
        dest='__karyn_pod_pruned__',
        type=int,
        nargs='+',
        help='pod pruned k-ary n-tree (only supported for "--tn 3"; must provide number pods and number spines, e.g.: "3 8")',
        metavar='N',
        default=[-1, -1])
    arg_parser.add_argument(
        '--ml',
        dest='__karyn_ml__',
        action='store_true',
        help='enable multi-link configuration between switches to fillup empty ports',
        default=False)


def _get_local_options(args):
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    half_radix = args.get('__karyn_half_radix__')
    if half_radix < 1:
        exit('ERR: k of the k-ary n-tree (--tk) must be positive')
    levels = args.get('__karyn_lvls__')
    if levels < 1:
        exit('ERR: n of the k-ary n-tree (--tn) must be positive')
    spine_pruned = args.get('__karyn_spine_pruned__')
    pod_pruned = args.get('__karyn_pod_pruned__')
    if len(pod_pruned) != 2 or \
            (pod_pruned[0] < 1 and pod_pruned[0] != -1) or \
            (pod_pruned[1] < 1 and pod_pruned[1] != -1):
        exit('ERR: pp of the k-ary n-tree (--tpp) must be exactly two positive integer')
    multi_link = args.get('__karyn_ml__')

    return (half_radix, levels, spine_pruned, pod_pruned, multi_link)


def _get_switch_name(prefix, w, lvl):
    return '%s<%s,%i>' % (prefix, ','.join([str(x) for x in w]), lvl)


def generate_topology(args={}):
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    topology = {}
    ports = {}

    if args.get('__argparse_used__'):
        k, n, spine_pruned, pod_pruned, multi_link = _get_local_options(args)
        do_port_matching = False
    else:
        do_port_matching = args.get('include_ports')
        if do_port_matching is None:
            do_port_matching = False
        elif not isinstance(do_port_matching, bool):
            exit('ERR: include_ports must be of type bool')

        k = args.get('k')
        if k is None:
            k = 4
        elif not isinstance(k, int) or k < 1:
            exit('ERR: k must be a positive integer')

        n = args.get('n')
        if n is None:
            n = 2
        elif not isinstance(n, int) or n < 1:
            exit('ERR: n must be a positive integer')

        spine_pruned = args.get('spine_pruned')
        if spine_pruned is None:
            spine_pruned = False
        elif not isinstance(spine_pruned, bool):
            exit('ERR: spine_pruned must be a bool type')

        pod_pruned = args.get('pod_pruned')
        if pod_pruned is None:
            pod_pruned = [-1, -1]
        elif not all(isinstance(x, int) for x in pod_pruned) or \
                len(pod_pruned) != 2 or min(pod_pruned) < 1:
            exit('ERR: pod_pruned must be a list of two positive integers')

    num_terminals, num_ports_per_terminal, terminal_prefix, \
        num_ports_per_switch, switch_prefix = _extract_basic_config(args)

    if num_terminals == -1:
        num_terminals = pow(k, n - 1)

    if num_ports_per_switch == -1:
        num_ports_per_switch = k * 2

    n_tuples = []
    w = [0 for x in range(n - 1)]
    n_tuples.append(w[:])
    for j in range(pow(k, n - 1)):
        for i in range(len(w)):
            w[i] += 1
            if w[i] == k:
                w[i] = 0
            else:
                n_tuples.append(w[:])
                break
    if pod_pruned[0] > 0 and n == 3:
        n_tuples = [t for t in n_tuples if t[0] < pod_pruned[0]]

    # generate all switches <w,lvl>
    for lvl in range(n):
        if lvl == 0:
            sw_guid = pow(2, 33)
        elif lvl == 1:
            sw_guid = pow(2, 33) - 26
        for w in n_tuples:
            switch = _get_switch_name(switch_prefix, w, lvl)
            topology[switch] = {'guid': sw_guid,
                                'radix': num_ports_per_switch,
                                'adj': [],
                                'is_switch': True}
            ports[switch] = num_ports_per_switch
            if lvl == 0:
                topology[switch]['is_root'] = True
                sw_guid += 26
            else:
                sw_guid -= 26

    # connect all switches
    if multi_link:
        t_ports = ceil(num_terminals / len(n_tuples)) * num_ports_per_terminal
        num_multi_link = max(1,
                             floor((num_ports_per_switch - max(t_ports, k))
                                   / k))
    else:
        num_multi_link = 1
    for lvl in range(n - 1):
        for w1 in n_tuples:
            for w2 in n_tuples:
                subw1, subw2 = w1[:lvl], w2[:lvl]
                subw1.extend(w1[lvl + 1:])
                subw2.extend(w2[lvl + 1:])
                if subw1 == subw2:
                    switch1 = _get_switch_name(switch_prefix, w1, lvl)
                    switch2 = _get_switch_name(switch_prefix, w2, lvl + 1)
                    topology[switch1]['adj'] += num_multi_link*[switch2]
                    ports[switch1] -= num_multi_link
                    ports[switch2] -= num_multi_link

    if pod_pruned[1] > 0 and n == 3:
        spines = [sw for sw in topology if ',0>' in sw]
        spines.sort(key=lambda x: tuple(
            [int(y)
             for y in x.replace(switch_prefix + '<', '').split(',')[0:-1]]))
        idx = 0
        for del_idx in range(pod_pruned[1], len(spines)):
            topology[spines[idx]]['adj'] += topology[spines[del_idx]]['adj']
            ports[spines[idx]] -= len(topology[spines[del_idx]]['adj'])
            del topology[spines[del_idx]]
            del ports[spines[del_idx]]
            idx = (idx + 1) % pod_pruned[1]
    elif spine_pruned:
        # for real networks we use 1/2 switches in the top layer, otherwise we
        # waste ports
        spines = [sw for sw in topology if ',0>' in sw]
        spines.sort(key=lambda x: tuple(
            [int(y)
             for y in x.replace(switch_prefix + '<', '').split(',')[0:-1]]))
        # take links from 2. half and add them to first half
        half = len(spines) // 2
        for idx in range(half):
            topology[spines[idx]]['adj'] += topology[spines[idx + half]]['adj']
            ports[spines[idx]] -= len(topology[spines[idx + half]]['adj'])
            del topology[spines[idx + half]]
            del ports[spines[idx + half]]

    # connect terminals
    set_term = 0
    while set_term < num_terminals:
        for w in n_tuples:
            switch = _get_switch_name(switch_prefix, w, n - 1)
            terminal = _get_terminal_name(terminal_prefix, set_term)
            topology[terminal] = {'guid': 4 * (set_term + 1),
                                  'radix': num_ports_per_terminal,
                                  'adj': []}
            for x in range(num_ports_per_terminal):
                topology[switch]['adj'].append(terminal)
                ports[switch] -= 1
            set_term += 1
            if set_term >= num_terminals:
                break

    # is the topology valid?
    if not _topology_is_valid(topology, ports, switch_prefix):
        exit('ERR: not enough ports per switch for all links and terminals')
    elif do_port_matching:
        _topology_port_matching(topology)

    return topology


if __name__ == "__main__":
    exit('ERR: not meant to be executed as stand-alone')
