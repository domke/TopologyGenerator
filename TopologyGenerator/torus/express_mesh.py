#!/usr/bin/env python3

from sys import exit, stderr
from argparse import _SubParsersAction
from ..common import _topology_is_valid, _extract_basic_config
from ..common import _get_terminal_name
from .torus import _generate_mesh_and_torus


def _set_local_options(arg_subparser):
    if not isinstance(arg_subparser, _SubParsersAction):
        exit('ERR: needs to be called with existing SubArgumentParser object')

    arg_parser = arg_subparser.add_parser(
        'expmesh',
        description='n-dimensional ExpressMesh topology with gap g (see: "Partitioning Low-diameter Networks to Eliminate Inter-job Interference", page 2, sec. 2 for details)')
    arg_parser.add_argument(
        '--dim',
        dest='__expmesh_dims__',
        type=int,
        nargs='+',
        help='list of the mesh sizes in each dimension [default: 5 5 (for 5x5-ExpressMesh)]',
        metavar='N',
        default=[5, 5])
    arg_parser.add_argument(
        '--gap',
        dest='__expmesh_gap__',
        type=int,
        help='gap to bridge larger distances by adding shortcut links [default: 2]',
        metavar='N',
        default=2)
    arg_parser.add_argument(
        '--ml',
        dest='__expmesh_ml__',
        action='store_true',
        help='enable multi-link configuration between switches to fillup empty ports',
        default=False)


def _get_local_options(args):
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    dims = args.get('__expmesh_dims__')
    gap = args.get('__expmesh_gap__')
    multi_link = args.get('__expmesh_ml__')

    return (dims, gap, multi_link)


def generate_topology(args={}):
    """
    default 5x5 ExpressMesh with single link between adjacent switches and
    additional links bridging a gap of 2
    """
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    if args.get('__argparse_used__'):
        dims, gap, multi_link = _get_local_options(args)
    else:
        dims = args.get('dimensions')
        if dims is None:
            dims = [3, 3]
        elif not all(isinstance(d, int) for d in dims) or min(dims) < 0:
            exit('ERR: dimensions must be a list of positive integers')

        gap = args.get('gap')
        if gap is None:
            gap = 2
        elif not isinstance(gap, int) or gap < 1:
            exit('ERR: gap must be a positive integer')

        multi_link = args.get('multi_link')
        if multi_link is None:
            multi_link = False
        elif not isinstance(multi_link, bool):
            exit('ERR: multi_link must be of type bool')

    if len(dims) < 1:
        exit('ERR: at least one positive dimension size must be given')
    if min(dims) < 2:
        stderr.write('WRN: sizes < 2 for mesh dimensions will be ignored\n')
        dims = [dim for dim in dims if dim > 1]
    dims.sort(reverse=True)

    if gap + 2 > dims[0]:
        exit('ERR: gap parameter too big and will not influence mesh topology')

    return _generate_mesh_and_torus(args, dims, gap, False, multi_link, False)


if __name__ == "__main__":
    exit('ERR: not meant to be executed as stand-alone')
