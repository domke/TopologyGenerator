#!/usr/bin/env python3

from sys import exit, stderr
from argparse import _SubParsersAction
from ..common import _topology_is_valid
from ..common import _get_terminal_name
from ..common import _extract_basic_config


def _set_local_options(arg_subparser=None):
    if not isinstance(arg_subparser, _SubParsersAction):
        exit('ERR: needs to be called with existing SubArgumentParser object')

    arg_parser = arg_subparser.add_parser(
        'tofu',
        description='6D mesh/torus topology (see Ajima et al. "Tofu: Interconnect for the K computer" for details)')
    arg_parser.add_argument(
        '--dims',
        dest='__tofu_dims__',
        type=int,
        nargs='+',
        help='list of the dimensions (XYZ) for the 3D torus containing tofu units [default: 1 1 1 (for 1 ABC unit)]',
        metavar='N',
        default=[1, 1, 1])
    arg_parser.add_argument(
        '--ext',
        dest='__tofu_extended_material__',
        action='store_true',
        help='generate supplement material (i.e., Tofu unicast routing tables)',
        default=False)


def _get_local_options(args):
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    dims = args.get('__tofu_dims__')
    include_suppl = args.get('__tofu_extended_material__')

    return (dims, include_suppl)


def _generate_tofu_routing_lft():
    return None


def _get_osm_torus2qos_routing_config(
        topology, dims, num_switches, switch_prefix, num_ports_per_switch):
    if not isinstance(topology, dict) or not all(
            isinstance(d, int) for d in dims):
        exit('ERR: invalid function parameter type(s)')

    if len(dims) != 6:
        return None

    torus2qos = {}
    torus2qos['topo'] = 'torus'
    torus2qos['portgroup_max_ports'] = num_ports_per_switch
    torus2qos['xradix'] = dims[0] * dims[3]  # dim X * A
    torus2qos['yradix'] = dims[1] * dims[4]  # dim Y * B
    torus2qos['zradix'] = dims[2] * dims[5]  # dim Z * C
    torus2qos['seed'] = {}

    for sw_idx in range(num_switches):
        idx_vec = _get_switch_idx_vector(dims, sw_idx)
        switch = _get_switch_name(switch_prefix, idx_vec)
        torus2qos[switch] = {'guid': topology[switch]['guid']}

    # generate list with torus-2qos switches for seed, take corner and middle
    # (so 9 at most; always w/ a,b,c=0)
    mid_dims = [(dims[0] - 1) / 2, (dims[1] - 1) / 2, (dims[2] - 1) / 2]
    seed_switches = set([
        (int(mid_dims[0]), int(mid_dims[1]), int(mid_dims[2])),
        (0, 0, 0),
        (dims[0] - 1, 0, 0),
        (0, dims[1] - 1, 0),
        (0, 0, dims[2] - 1),
        (dims[0] - 1, dims[1] - 1, 0),
        (dims[0] - 1, 0, dims[2] - 1),
        (0, dims[1] - 1, dims[2] - 1),
        (dims[0] - 1, dims[1] - 1, dims[2] - 1)])

    for i in range(len(seed_switches)):
        idx_vec = 6 * [0]
        idx_vec[0], idx_vec[1], idx_vec[2] = seed_switches.pop()
        switch = _get_switch_name(switch_prefix, idx_vec)

        xp = _get_switch_name(switch_prefix,
                              [(idx_vec[0] + 1) % dims[0]] + idx_vec[1:])
        xm = _get_switch_name(switch_prefix,
                              [(idx_vec[0] - 1) % dims[0]] + idx_vec[1:])
        yp = _get_switch_name(switch_prefix,
                              idx_vec[:1] + [(idx_vec[1] + 1) % dims[1]] + idx_vec[2:])
        ym = _get_switch_name(switch_prefix,
                              idx_vec[:1] + [(idx_vec[1] - 1) % dims[1]] + idx_vec[2:])
        zp = _get_switch_name(switch_prefix,
                              idx_vec[:2] + [(idx_vec[2] + 1) % dims[2]] + idx_vec[3:])
        zm = _get_switch_name(switch_prefix,
                              idx_vec[:2] + [(idx_vec[2] - 1) % dims[2]] + idx_vec[3:])

        torus2qos['seed'][switch] = {}
        if dims[0] > 1:
            torus2qos['seed'][switch]['xp_link'] = xp
            torus2qos['seed'][switch]['xm_link'] = xm
        else:
            xp = _get_switch_name(switch_prefix,
                                  idx_vec[:3] + [(idx_vec[3] + 1) % dims[3]] + idx_vec[4:])
            torus2qos['seed'][switch]['xp_link'] = xp
        if dims[1] > 1:
            torus2qos['seed'][switch]['yp_link'] = yp
            torus2qos['seed'][switch]['ym_link'] = ym
        else:
            yp = _get_switch_name(switch_prefix,
                                  idx_vec[:4] + [(idx_vec[4] + 1) % dims[4]] + idx_vec[5:])
            ym = _get_switch_name(switch_prefix,
                                  idx_vec[:4] + [(idx_vec[4] - 1) % dims[4]] + idx_vec[5:])
            torus2qos['seed'][switch]['yp_link'] = yp
            torus2qos['seed'][switch]['ym_link'] = ym
        if dims[2] > 1:
            torus2qos['seed'][switch]['zp_link'] = zp
            torus2qos['seed'][switch]['zm_link'] = zm
        else:
            zp = _get_switch_name(switch_prefix,
                                  idx_vec[:5] + [(idx_vec[5] + 1) % dims[5]])
            torus2qos['seed'][switch]['zp_link'] = zp
        torus2qos['seed'][switch]['x_dateline'] = -1 * idx_vec[0]
        torus2qos['seed'][switch]['y_dateline'] = -1 * idx_vec[1]
        torus2qos['seed'][switch]['z_dateline'] = -1 * idx_vec[2]

    return torus2qos


def _get_osm_dor_routing_config(topology, dims, num_switches, switch_prefix):
    if not isinstance(topology, dict) or not all(
            isinstance(d, int) for d in dims):
        exit('ERR: invalid function parameter type(s)')

    if len(dims) != 6:
        return None

    dor = {}

    for sw_idx in range(num_switches):
        idx_vec = _get_switch_idx_vector(dims, sw_idx)
        switch = _get_switch_name(switch_prefix, idx_vec)
        sw_idx_a, sw_idx_b, sw_idx_c = idx_vec[3:6]
        dor[switch] = {'guid': topology[switch]['guid'], 'dimList': []}

        xp = _get_switch_name(switch_prefix,
                              [(idx_vec[0] + 1) % dims[0]] + idx_vec[1:])
        xm = _get_switch_name(switch_prefix,
                              [(idx_vec[0] - 1) % dims[0]] + idx_vec[1:])
        yp = _get_switch_name(switch_prefix,
                              idx_vec[:1] + [(idx_vec[1] + 1) % dims[1]] + idx_vec[2:])
        ym = _get_switch_name(switch_prefix,
                              idx_vec[:1] + [(idx_vec[1] - 1) % dims[1]] + idx_vec[2:])
        zp = _get_switch_name(switch_prefix,
                              idx_vec[:2] + [(idx_vec[2] + 1) % dims[2]] + idx_vec[3:])
        zm = _get_switch_name(switch_prefix,
                              idx_vec[:2] + [(idx_vec[2] - 1) % dims[2]] + idx_vec[3:])
        ap = _get_switch_name(switch_prefix,
                              idx_vec[:3] + [(idx_vec[3] + 1) % dims[3]] + idx_vec[4:])
        am = _get_switch_name(switch_prefix,
                              idx_vec[:3] + [(idx_vec[3] - 1) % dims[3]] + idx_vec[4:])
        bp = _get_switch_name(switch_prefix,
                              idx_vec[:4] + [(idx_vec[4] + 1) % dims[4]] + idx_vec[5:])
        bm = _get_switch_name(switch_prefix,
                              idx_vec[:4] + [(idx_vec[4] - 1) % dims[4]] + idx_vec[5:])
        cp = _get_switch_name(switch_prefix,
                              idx_vec[:5] + [(idx_vec[5] + 1) % dims[5]])
        cm = _get_switch_name(switch_prefix,
                              idx_vec[:5] + [(idx_vec[5] - 1) % dims[5]])

        dor[switch]['dimList'] = [xp, xm, yp, ym, zp, zm]
        if sw_idx_a + 1 < 2:
            dor[switch]['dimList'].append(ap)
        if sw_idx_a - 1 >= 0:
            dor[switch]['dimList'].append(am)
        dor[switch]['dimList'].append(bp)
        dor[switch]['dimList'].append(bm)
        if sw_idx_c + 1 < 2:
            dor[switch]['dimList'].append(cp)
        if sw_idx_c - 1 >= 0:
            dor[switch]['dimList'].append(cm)

    return dor


def _get_switch_name(prefix, id_vector):
    id_vector = id_vector[:3] + [id_vector[3] + 2*id_vector[5] + 4*id_vector[4]]
    return '%s<%s>' % (prefix, ','.join([str(x) for x in id_vector]))


def _get_switch_idx_vector(dims, sw_idx):
    if len(dims) != 6:
        exit('ERR: invalid function parameter type(s)')

    idx_vector = len(dims) * [0]

    for dim in range(3, len(dims)):
        idx_vector[dim] = sw_idx % dims[dim]
        sw_idx = int(sw_idx / dims[dim])
    for dim in range(0, 3):
        idx_vector[dim] = sw_idx % dims[dim]
        sw_idx = int(sw_idx / dims[dim])

    return idx_vector


def _generate_tofu_topology_and_routing(
        args, dims, include_supplement_material=False, do_port_matching=False):
    if not isinstance(args, dict) or not all(isinstance(d, int)
                                             for d in dims) or len(dims) != 6:
        exit('ERR: invalid function parameter type(s)')

    topology = {}
    ports = {}

    num_terminals, num_ports_per_terminal, terminal_prefix, \
        num_ports_per_switch, switch_prefix = _extract_basic_config(args)

    num_switches = 1
    for dim in dims:
        num_switches *= dim

    if num_terminals != -1 and num_terminals != num_switches:
        stderr.write(
            'WRN: requested number of terminals unsupported, resetting to one per switch')
    num_terminals = num_switches

    if num_ports_per_terminal != -1 and num_ports_per_terminal != 1:
        stderr.write(
            'WRN: requested number of ports per terminal unsupported, resetting to 1')
    num_ports_per_terminal = 1

    if num_ports_per_switch != -1 and num_ports_per_switch < 11:
        stderr.write(
            'WRN: requested number of ports per switch unsupported, resetting to 11')
    num_ports_per_switch = 11

    # generate all switches in the integer lattice
    sw_guid = pow(2, 33)
    for sw_idx in range(num_switches):
        idx_vector = _get_switch_idx_vector(dims, sw_idx)
        switch = _get_switch_name(switch_prefix, idx_vector)
        topology[switch] = {'guid': sw_guid,
                            'radix': num_ports_per_switch,
                            'adj': [],
                            'is_switch': True}
        ports[switch] = num_ports_per_switch
        # there's no real root/spine for tofu, so use first sw if needed
        if sw_idx == 0:
            topology[switch]['fake_root_switch'] = True
        sw_guid -= 26

    # connect all switches
    for sw_idx in range(num_switches):
        idx_vector_1 = _get_switch_idx_vector(dims, sw_idx)
        switch1 = _get_switch_name(switch_prefix, idx_vector_1)

        # get the idx w.r.t A, B, and C dim within the unit
        sw_idx_a, sw_idx_b, sw_idx_c = idx_vector_1[3:6]

        # first connect the switch to its neighbors in the same tofu unit
        # connect mesh-like in A dim
        for adj_sw_idx_a in range(sw_idx_a + 1, 2):
            idx_vector_2 = idx_vector_1[:3] + [adj_sw_idx_a] + idx_vector_1[4:]
            switch2 = _get_switch_name(switch_prefix, idx_vector_2)
            topology[switch1]['adj'].append(switch2)
            ports[switch1] -= 1
            ports[switch2] -= 1

        # connect torus-like in B dim
        for adj_sw_idx_b in range(sw_idx_b + 1, 3):
            idx_vector_2 = idx_vector_1[:4] + [adj_sw_idx_b] + idx_vector_1[5:]
            switch2 = _get_switch_name(switch_prefix, idx_vector_2)
            topology[switch1]['adj'].append(switch2)
            ports[switch1] -= 1
            ports[switch2] -= 1

        # connect mesh-like in C dim
        for adj_sw_idx_c in range(sw_idx_c + 1, 2):
            idx_vector_2 = idx_vector_1[:5] + [adj_sw_idx_c]
            switch2 = _get_switch_name(switch_prefix, idx_vector_2)
            topology[switch1]['adj'].append(switch2)
            ports[switch1] -= 1
            ports[switch2] -= 1

        # and now connect the switch to neighboring units in the tofu
        # connect to neighbor units in X
        if dims[0] > 1:
            idx_vector_2 = idx_vector_1[:]
            idx_vector_2[0] = (idx_vector_2[0] + 1) % dims[0]
            switch2 = _get_switch_name(switch_prefix, idx_vector_2)
            topology[switch1]['adj'].append(switch2)
            ports[switch1] -= 1
            ports[switch2] -= 1

        # connect to neighbor units in Y
        if dims[1] > 1:
            idx_vector_2 = idx_vector_1[:]
            idx_vector_2[1] = (idx_vector_2[1] + 1) % dims[1]
            switch2 = _get_switch_name(switch_prefix, idx_vector_2)
            topology[switch1]['adj'].append(switch2)
            ports[switch1] -= 1
            ports[switch2] -= 1

        # connect to neighbor units in X
        if dims[2] > 1:
            idx_vector_2 = idx_vector_1[:]
            idx_vector_2[2] = (idx_vector_2[2] + 1) % dims[2]
            switch2 = _get_switch_name(switch_prefix, idx_vector_2)
            topology[switch1]['adj'].append(switch2)
            ports[switch1] -= 1
            ports[switch2] -= 1

    # generate terminals and connect them to switches (round-robin)
    set_term = 0
    while set_term < num_terminals:
        for sw_idx in range(num_switches):
            idx_vector = _get_switch_idx_vector(dims, sw_idx)
            switch = _get_switch_name(switch_prefix, idx_vector)
            terminal = _get_terminal_name(terminal_prefix, set_term)
            topology[terminal] = {
                'guid': 4 * (set_term + 1),
                'radix': num_ports_per_terminal,
                'adj': []}
            for x in range(num_ports_per_terminal):
                topology[switch]['adj'].append(terminal)
                ports[switch] -= 1
            set_term += 1
            if set_term >= num_terminals:
                break

    # is the topology valid?
    if not _topology_is_valid(topology, ports, switch_prefix):
        exit('ERR: not enough ports per switch for all links and terminals')
    elif do_port_matching:
        _topology_port_matching(topology)

    if include_supplement_material:
        supplement_material = {}

        tofu_routing_lft = _generate_tofu_routing_lft()
        torus2qos_routing_config = _get_osm_torus2qos_routing_config(
            topology, dims, num_switches, switch_prefix, num_ports_per_switch)
        dor_routing_config = _get_osm_dor_routing_config(
            topology, dims, num_switches, switch_prefix)

        if tofu_routing_lft:
            supplement_material['tofu_routing_lft'] = tofu_routing_lft
        if torus2qos_routing_config:
            supplement_material[
                'osm_torus2qos_input'] = torus2qos_routing_config
        if dor_routing_config:
            supplement_material['osm_dor_input'] = dor_routing_config

        return (topology, supplement_material)

    return topology


def generate_topology(args={}):
    """
    default 2x3x2 mesh/torus ABC Tofu unit as basic building block for 6D Tofu
    """
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    if args.get('__argparse_used__'):
        dims, incl_suppl = _get_local_options(args)
        do_port_matching = False
    else:
        dims = args.get('dimensions')
        if dims is None:
            dims = [1, 1, 1]
        elif not all(isinstance(d, int) for d in dims) or min(dims) < 0:
            exit('ERR: dimensions must be a list of positive integers')

        do_port_matching = args.get('include_ports')
        if do_port_matching is None:
            do_port_matching = False
        elif not isinstance(do_port_matching, bool):
            exit('ERR: include_ports must be of type bool')

        incl_suppl = args.get('create_supplement_material')
        if incl_suppl is None:
            incl_suppl = False
        elif not isinstance(incl_suppl, bool):
            exit('ERR: create_supplement_material must be of type bool')

    # sanitize the dimesions array
    if len(dims) < 1:
        exit('ERR: at least one positive dimension size must be given')
    if min(dims) < 1:
        stderr.write('WRN: sizes < 1 for tofo dimensions will be ignored\n')
    dims = [dim for dim in dims if dim > 0]
    dims += [1 for dim in range(len(dims), 3)]
    if len(dims) > 3:
        exit('ERR: at most three positive dimension size should be given')

    # and now add the tofu unit (2x3x2 mesh/torus for ABC dimension)
    dims += [2, 3, 2]

    return _generate_tofu_topology_and_routing(
        args, dims, incl_suppl, do_port_matching)


if __name__ == "__main__":
    exit('ERR: not meant to be executed as stand-alone')
