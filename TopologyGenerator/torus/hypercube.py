#!/usr/bin/env python3

from sys import exit
from math import ceil
from argparse import _SubParsersAction
from ..common import _topology_is_valid, _extract_basic_config
from ..common import _get_terminal_name
from ..common import _topology_port_matching


def _set_local_options(arg_subparser):
    if not isinstance(arg_subparser, _SubParsersAction):
        exit('ERR: needs to be called with existing SubArgumentParser object')

    arg_parser = arg_subparser.add_parser(
        'hypercube',
        description='n-dimensional (enhanced) hypercube topology')
    arg_parser.add_argument(
        '--dim',
        dest='__hypercube_dim__',
        type=int,
        help='dimension parameter of the hypercude [default: 3 (-> cube)]',
        metavar='N',
        default=3)
    arg_parser.add_argument(
        '--ml',
        dest='__hypercube_ml__',
        action='store_true',
        help='enable multi-link configuration between switches to fillup empty ports (equal redundancy across all links)',
        default=False)
    arg_parser.add_argument(
        '--vml',
        dest='__hypercube_vml__',
        type=int,
        nargs='+',
        help='per dimension varying number of inter-switch links (enhanced -ml); missing N interpreted as 1',
        metavar='N',
        default=[])


def _get_local_options(args):
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    dim = args.get('__hypercube_dim__')
    if dim < 0:
        exit('ERR: hypercube dimension (--dim) must be positive')

    multi_link = args.get('__hypercube_ml__')
    per_dim_multi_link = args.get('__hypercube_vml__')

    return (dim, multi_link, per_dim_multi_link)


def _get_switch_name(prefix, id_vector):
    return '%s<%s>' % (prefix, ','.join([str(x) for x in id_vector]))


def _get_switch_idx_vector(L, sw_idx):
    idx_vector = L * [0]
    for dim in range(L):
        # labeling scheme: S<D1,D2,...,Dn>
        idx_vector[dim] = sw_idx % 2
        sw_idx = int(sw_idx / 2)
    return idx_vector


def generate_topology(args={}):
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    topology = {}
    ports = {}

    if args.get('__argparse_used__'):
        dim, multi_link, per_dim_ml = _get_local_options(args)
        do_port_matching = False
    else:
        do_port_matching = args.get('include_ports')
        if do_port_matching is None:
            do_port_matching = False
        elif not isinstance(do_port_matching, bool):
            exit('ERR: include_ports must be of type bool')

        dim = args.get('dimension')
        if not dim:
            dim = 3
        elif not isinstance(dim, int) or dim < 0:
            exit('ERR: dimension must be a positive integer')

        multi_link = args.get('multi_link')
        if multi_link is None:
            multi_link = False
        elif not isinstance(multi_link, bool):
            exit('ERR: multi_link must be of type bool')

        per_dim_ml = args.get('per_dim_multi_link')
        if per_dim_ml is None:
            per_dim_ml = []
        elif not all(isinstance(n, int) for n in per_dim_ml) or min(per_dim_ml) < 1:
            exit('ERR: per_dim_multi_link must be a list of positive integers')

    varying_multi_link = False
    if len(per_dim_ml) > 0:
        multi_link = False
        varying_multi_link = True
        if len(per_dim_ml) > dim:
            per_dim_ml = [per_dim_ml[i] for i in range(dim)]
        elif len(per_dim_ml) < dim:
            per_dim_ml += [1 for i in range(dim - len(per_dim_ml))]

    num_terminals, num_ports_per_terminal, terminal_prefix, \
        num_ports_per_switch, switch_prefix = _extract_basic_config(args)

    if num_terminals == -1:
        num_terminals = pow(2, dim)

    if num_ports_per_switch == -1:
        num_ports_per_switch = dim + int(ceil(num_terminals / pow(2, dim)))

    # generate all switches in the n-dim space
    sw_guid = pow(2, 33)
    for sw_idx in range(0, pow(2, dim)):
        idx_vector = _get_switch_idx_vector(dim, sw_idx)
        switch = _get_switch_name(switch_prefix, idx_vector)
        topology[switch] = {'guid': sw_guid,
                            'radix': num_ports_per_switch,
                            'adj': [],
                            'is_switch': True}
        ports[switch] = num_ports_per_switch
        # there is no real root/spine for hypercubes, so use first if needed
        if sw_idx == 0:
            topology[switch]['fake_root_switch'] = True
        sw_guid -= 26

    # generate terminals and connect them to switches
    set_term = 0
    while set_term < num_terminals:
        for sw_idx in range(0, pow(2, dim)):
            idx_vector = _get_switch_idx_vector(dim, sw_idx)
            switch = _get_switch_name(switch_prefix, idx_vector)
            terminal = _get_terminal_name(terminal_prefix, set_term)
            topology[terminal] = {
                'guid': 4 * (set_term + 1),
                'radix': num_ports_per_terminal,
                'adj': []}
            for x in range(num_ports_per_terminal):
                topology[switch]['adj'].append(terminal)
                ports[switch] -= 1
            set_term += 1
            if set_term >= num_terminals:
                break

    # connect all switches (multiple times if possible)
    while True:
        # check if we could potentially do another round
        for sw_idx_1 in range(0, pow(2, dim)):
            for sw_idx_2 in range(sw_idx_1 + 1, pow(2, dim)):
                idx_vector_1 = _get_switch_idx_vector(dim, sw_idx_1)
                idx_vector_2 = _get_switch_idx_vector(dim, sw_idx_2)
                diff = 0
                for i in range(len(idx_vector_1)):
                    if idx_vector_1[i] != idx_vector_2[i]:
                        diff += 1
                        diff_pos = i
                if diff > 1:
                    continue
                switch1 = _get_switch_name(switch_prefix, idx_vector_1)
                switch2 = _get_switch_name(switch_prefix, idx_vector_2)
                num_links = 1
                if varying_multi_link:
                    num_links = per_dim_ml[diff_pos]
                for x in range(num_links):
                    topology[switch1]['adj'].append(switch2)
                ports[switch1] -= num_links
                ports[switch2] -= num_links
        if not multi_link:
            break
        # break if some switches don't have enough free ports for another round
        if len([num_free for num_free in list(
                ports.values()) if num_free < dim]):
            break

    # is the topology valid?
    if not _topology_is_valid(topology, ports, switch_prefix):
        exit('ERR: not enough ports per switch for all links and terminals')
    elif do_port_matching:
        _topology_port_matching(topology)

    return topology


if __name__ == "__main__":
    exit('ERR: not meant to be executed as stand-alone')
