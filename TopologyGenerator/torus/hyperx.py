#!/usr/bin/env python3

from sys import exit, stderr
from math import ceil
from functools import reduce
from argparse import _SubParsersAction
from ..common import _topology_is_valid, _extract_basic_config
from ..common import _get_terminal_name
from ..common import _topology_port_matching


def _set_local_options(arg_subparser):
    if not isinstance(arg_subparser, _SubParsersAction):
        exit('ERR: needs to be called with existing SubArgumentParser object')

    arg_parser = arg_subparser.add_parser(
        'hyperx',
        description='HyperX(L,S,1,T) topology (regular; K==1; see "HyperX: Topology, Routing, and Packaging of Efficient Large-Scale Networks", page 3, sec. 3 for details)')
    arg_parser.add_argument(
        '--hxL',
        dest='__hyperx_L__',
        type=int,
        help='index space for the switches, i.e. L-dim integer lattice used [default: 2]',
        metavar='N',
        default=2)
    arg_parser.add_argument(
        '--hxS',
        dest='__hyperx_S__',
        type=int,
        nargs='+',
        help='index ranges in each dimension, i.e. 0...S_i-1 to identify switch position [default: 3 3]',
        metavar='N',
        default=[3, 3])
    arg_parser.add_argument(
        '--hxK',
        dest='__hyperx_K__',
        type=int,
        nargs='+',
        help='relative link bandwidths (thru link redundancy) in each dimension [default: 1 1]',
        metavar='N',
        default=[1, 1])
    arg_parser.add_argument(
        '--hxT',
        dest='__hyperx_T__',
        type=int,
        help='number of terminals per switch [default: 4]',
        metavar='N',
        default=1)


def _get_local_options(args):
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    L = args.get('__hyperx_L__')
    if L < 1:
        exit('ERR: index space (--hxL) must be positive')
    S = args.get('__hyperx_S__')
    if len(S) != L:
        exit('ERR: array size of index ranges (--hxS) must be equal to --hxL')
    K = args.get('__hyperx_K__')
    if len(K) != L:
        exit('ERR: array size of link bandwidths (--hxK) must be equal to --hxL')
    T = args.get('__hyperx_T__')
    if T < 1:
        exit('ERR: number of terminals per switch (--hxT) must be positive')

    return (L, S, K, T)


def _get_switch_name(prefix, id_vector):
    return '%s<%s>' % (prefix, ','.join([str(x) for x in id_vector]))


def _get_switch_idx_vector(L, S, sw_idx):
    idx_vector = L * [0]

    for dim in range(L):
        idx_vector[dim] = sw_idx % S[dim]
        sw_idx = int(sw_idx / S[dim])

    return idx_vector


def generate_topology(args={}):
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    topology = {}
    ports = {}

    if args.get('__argparse_used__'):
        L, S, K, T = _get_local_options(args)
        do_port_matching = False
    else:
        do_port_matching = args.get('include_ports')
        if do_port_matching is None:
            do_port_matching = False
        elif not isinstance(do_port_matching, bool):
            exit('ERR: include_ports must be of type bool')

        L = args.get('L')
        if L is None:
            L = 2
        elif not isinstance(L, int) or L < 1:
            exit('ERR: L must be a positive integer')

        S = args.get('S')
        if S is None:
            S = [3, 3]
        elif not all(isinstance(s, int) for s in S) or min(S) < 0:
            exit('ERR: S must be a list of positive integers')

        K = args.get('K')
        if K is None:
            K = [1, 1]
        elif not all(isinstance(k, int) for k in K) or min(K) < 0:
            exit('ERR: K must be a list of positive integers')

        T = args.get('T')
        if T is None:
            T = 4
        elif not isinstance(T, int) or T < 1:
            exit('ERR: T must be a positive integer')

    num_terminals, num_ports_per_terminal, terminal_prefix, \
        num_ports_per_switch, switch_prefix = _extract_basic_config(args)

    if len(S) < 1:
        exit('ERR: at least one positive dimension size must be given')
    if min(S) < 2:
        stderr.write('WRN: dimension sizes < 2 will be ignored\n')
        S = [s for s in S if s > 1]

    if len(K) < 1:
        stderr.write('WRN: no link bandwidths given, so assuming 1 for all\n')
        K = [1 for s in S]
    if min(K) < 1:
        stderr.write('WRN: bandwidth demands < 1 will be replaced by 1\n')
        K = [k if k > 1 else 1 for k in K]

    num_switches = reduce(lambda x, y: x * y, S)

    if num_terminals == -1:
        num_terminals = T * num_switches
    else:
        T = int(ceil(num_terminals / num_switches))
        stderr.write(
            'WRN: changing T to %i based on provided num_terminals parameter\n' %
            T)

    if num_ports_per_switch == -1:
        num_ports_per_switch = T + \
            reduce(lambda x, y: x + y, [K[l] * (S[l] - 1) for l in range(L)])

    # generate all switches in the L-dim integer lattice
    sw_guid = pow(2, 33)
    for sw_idx in range(0, num_switches):
        idx_vector = _get_switch_idx_vector(L, S, sw_idx)
        switch = _get_switch_name(switch_prefix, idx_vector)
        topology[switch] = {'guid': sw_guid,
                            'radix': num_ports_per_switch,
                            'adj': [],
                            'is_switch': True}
        ports[switch] = num_ports_per_switch
        # there is no real root/spine for HyperX, so use the first sw if needed
        if sw_idx == 0:
            topology[switch]['fake_root_switch'] = True
        sw_guid -= 26

    # generate terminals and connect them to switches
    # (round-robin until num_terminals satisfied or until hxT satisfied)
    set_term = 0
    round = 0
    while set_term < num_terminals and round < T:
        for sw_idx in range(0, num_switches):
            idx_vector = _get_switch_idx_vector(L, S, sw_idx)
            switch = _get_switch_name(switch_prefix, idx_vector)
            terminal = _get_terminal_name(terminal_prefix, set_term)
            topology[terminal] = {'guid': 4 * (set_term + 1),
                                  'radix': num_ports_per_terminal,
                                  'adj': []}
            for x in range(num_ports_per_terminal):
                topology[switch]['adj'].append(terminal)
                ports[switch] -= 1
            set_term += 1
            if set_term >= num_terminals:
                break
        round += 1

    # connect all switches (paper says: fully connected in each dimension)
    # (a switch connects to all others whose multi-index is the same in all
    #  but one coordinate)
    for sw_idx_1 in range(0, num_switches):
        for sw_idx_2 in range(sw_idx_1 + 1, num_switches):
            idx_vector_1 = _get_switch_idx_vector(L, S, sw_idx_1)
            idx_vector_2 = _get_switch_idx_vector(L, S, sw_idx_2)
            diff = 0
            for i in range(len(idx_vector_1)):
                if idx_vector_1[i] != idx_vector_2[i]:
                    diff += 1
                    dim = i
            if diff > 1:
                continue
            switch1 = _get_switch_name(switch_prefix, idx_vector_1)
            switch2 = _get_switch_name(switch_prefix, idx_vector_2)
            for k in range(K[dim]):
                topology[switch1]['adj'].append(switch2)
                ports[switch1] -= 1
                ports[switch2] -= 1

    # is the topology valid?
    if not _topology_is_valid(topology, ports, switch_prefix):
        exit('ERR: not enough ports per switch for all links and terminals')
    elif do_port_matching:
        _topology_port_matching(topology)

    return topology


if __name__ == "__main__":
    exit('ERR: not meant to be executed as stand-alone')
