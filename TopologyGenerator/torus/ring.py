#!/usr/bin/env python3

from sys import exit, stderr
from argparse import _SubParsersAction
from ..common import _topology_is_valid, _extract_basic_config
from ..common import _get_terminal_name
from .torus import _generate_mesh_and_torus


def _set_local_options(arg_subparser):
    if not isinstance(arg_subparser, _SubParsersAction):
        exit('ERR: needs to be called with existing SubArgumentParser object')

    arg_parser = arg_subparser.add_parser(
        'ring',
        description='Ring topology')
    arg_parser.add_argument(
        '--ns',
        dest='__ring_num_switches__',
        type=int,
        help='number of switches in the ring [default: 5]',
        metavar='N',
        default=5)
    arg_parser.add_argument(
        '--ml',
        dest='__ring_ml__',
        action='store_true',
        help='enable multi-link configuration between switches to fillup empty ports',
        default=False)


def _get_local_options(args):
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    num_switches = args.get('__ring_num_switches__')
    if num_switches < 1:
        exit('ERR: number of switches (--ns) must be positive')

    multi_link = args.get('__ring_ml__')

    return ([num_switches], multi_link)


def generate_topology(args={}):
    """
    default 5-node ring topology
    """
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    if args.get('__argparse_used__'):
        dim, multi_link = _get_local_options(args)
    else:
        num_switches = args.get('num_switches')
        if num_switches is None:
            dim = [5]
        elif not isinstance(num_switches, int) or num_switches < 0:
            exit('ERR: number of switches must be a positive integer')
        else:
            dim = [num_switches]

        multi_link = args.get('multi_link')
        if multi_link is None:
            multi_link = False
        elif not isinstance(multi_link, bool):
            exit('ERR: multi_link must be of type bool')

    return _generate_mesh_and_torus(args, dim, 0, True, multi_link)


if __name__ == "__main__":
    exit('ERR: not meant to be executed as stand-alone')
