#!/usr/bin/env python3

from sys import exit, stderr
from math import ceil
from argparse import _SubParsersAction
from ..common import _topology_is_valid, _extract_basic_config
from ..common import _get_terminal_name
from ..common import _topology_port_matching


def _set_local_options(arg_subparser):
    if not isinstance(arg_subparser, _SubParsersAction):
        exit('ERR: needs to be called with existing SubArgumentParser object')

    arg_parser = arg_subparser.add_parser(
        'torus',
        description='n-dimensional torus topology')
    arg_parser.add_argument(
        '--dim',
        dest='__torus_dims__',
        type=int,
        nargs='+',
        help='list of the torus sizes in each dimension [default: 3 2 (for 3x2-torus)]',
        metavar='N',
        default=[3, 2])
    arg_parser.add_argument(
        '--ml',
        dest='__torus_ml__',
        action='store_true',
        help='enable multi-link configuration between switches to fillup empty ports',
        default=False)
    arg_parser.add_argument(
        '--ext',
        dest='__torus_extended_material__',
        action='store_true',
        help='generate supplement material (i.e., DOR/Torus2QoS routing input for OpenSM; only applies to 2D and 3D tori)',
        default=False)


def _get_local_options(args):
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    dims = args.get('__torus_dims__')
    multi_link = args.get('__torus_ml__')
    include_suppl = args.get('__torus_extended_material__')

    return (dims, multi_link, include_suppl)


def _get_switch_name(prefix, id_vector):
    return '%s<%s>' % (prefix, ','.join([str(x) for x in id_vector]))


def _get_switch_idx_vector(dims, sw_idx):
    idx_vector = len(dims) * [0]

    for i in range(len(dims)):
        idx_vector[i] = sw_idx % dims[i]
        sw_idx = int(sw_idx / dims[i])

    return idx_vector


def _get_osm_torus2qos_routing_config(
        topology, dims, wrap_around_links, switch_prefix, num_ports_per_switch):
    if not isinstance(topology, dict) or not all(
            isinstance(d, int) for d in dims):
        exit('ERR: invalid function parameter type(s)')

    if len(dims) < 2 or len(dims) > 3:
        return None

    torus2qos = {}
    if wrap_around_links:
        torus2qos['topo'] = 'torus'
    else:
        torus2qos['topo'] = 'mesh'

    torus2qos['portgroup_max_ports'] = num_ports_per_switch
    torus2qos['xradix'] = dims[0]
    torus2qos['yradix'] = dims[1]
    torus2qos['zradix'] = 1
    if len(dims) == 3:
        torus2qos['zradix'] = dims[2]
    torus2qos['seed'] = {}

    # generate list with torus-2qos switches for seeding:
    #   take middle and all corners (so 5 at most for 2d and 9 at most for 3d)
    seed_switches = []
    if len(dims) == 2:
        seed_switches.append([int((dims[0] - 1) / 2), int((dims[1] - 1) / 2)])
        seed_switches.append([0, 0])
        seed_switches.append([0, dims[1] - 1])
        seed_switches.append([dims[0] - 1, 0])
        seed_switches.append([dims[0] - 1, dims[1] - 1])
    else:
        seed_switches.append(
            [int((dims[0] - 1) / 2),
             int((dims[1] - 1) / 2),
             int((dims[2] - 1) / 2)])
        seed_switches.append([0, 0, 0])
        seed_switches.append([dims[0] - 1, 0, 0])
        seed_switches.append([0, dims[1] - 1, 0])
        seed_switches.append([0, 0, dims[2] - 1])
        seed_switches.append([dims[0] - 1, dims[1] - 1, 0])
        seed_switches.append([dims[0] - 1, 0, dims[2] - 1])
        seed_switches.append([0, dims[1] - 1, dims[2] - 1])
        seed_switches.append([dims[0] - 1, dims[1] - 1, dims[2] - 1])

    for x in range(len(seed_switches)):
        idx_vector = seed_switches.pop()
        switch = _get_switch_name(switch_prefix, idx_vector)
        torus2qos['seed'][switch] = {}

        # get both neighbor on x, y, (z) axis with current position +/- 1
        xp_link_vec = idx_vector[:]
        xp_link_vec[0] = (xp_link_vec[0] + 1) % dims[0]
        xp_link_sw = _get_switch_name(switch_prefix, xp_link_vec)
        xm_link_vec = idx_vector[:]
        xm_link_vec[0] = (xm_link_vec[0] - 1) % dims[0]
        xm_link_sw = _get_switch_name(switch_prefix, xm_link_vec)

        yp_link_vec = idx_vector[:]
        yp_link_vec[1] = (yp_link_vec[1] + 1) % dims[1]
        yp_link_sw = _get_switch_name(switch_prefix, yp_link_vec)
        ym_link_vec = idx_vector[:]
        ym_link_vec[1] = (ym_link_vec[1] - 1) % dims[1]
        ym_link_sw = _get_switch_name(switch_prefix, ym_link_vec)

        if len(dims) > 2:
            zp_link_vec = idx_vector[:]
            zp_link_vec[2] = (zp_link_vec[2] + 1) % dims[2]
            zp_link_sw = _get_switch_name(switch_prefix, zp_link_vec)
            zm_link_vec = idx_vector[:]
            zm_link_vec[2] = (zm_link_vec[2] - 1) % dims[2]
            zm_link_sw = _get_switch_name(switch_prefix, zm_link_vec)

        if wrap_around_links:
            torus2qos['seed'][switch]['xp_link'] = xp_link_sw
            torus2qos['seed'][switch]['xm_link'] = xm_link_sw
            torus2qos['seed'][switch]['yp_link'] = yp_link_sw
            torus2qos['seed'][switch]['ym_link'] = ym_link_sw
            if len(dims) > 2:
                torus2qos['seed'][switch]['zp_link'] = zp_link_sw
                torus2qos['seed'][switch]['zm_link'] = zm_link_sw
        else:
            if xp_link_vec[0] < dims[0]:
                torus2qos['seed'][switch]['xp_link'] = xp_link_sw
            if xm_link_vec[0] >= 0:
                torus2qos['seed'][switch]['xm_link'] = xm_link_sw
            if yp_link_vec[1] < dims[1]:
                torus2qos['seed'][switch]['yp_link'] = yp_link_sw
            if ym_link_vec[1] >= 0:
                torus2qos['seed'][switch]['ym_link'] = ym_link_sw
            if len(dims) > 2:
                if zp_link_vec[2] < dims[2]:
                    torus2qos['seed'][switch]['zp_link'] = zp_link_sw
                if zm_link_vec[2] >= 0:
                    torus2qos['seed'][switch]['zm_link'] = zm_link_sw

    return torus2qos


def _get_osm_dor_routing_config(
        topology, dims, num_switches, switch_prefix, wrap_around_links):
    if not isinstance(topology, dict) or not all(
            isinstance(d, int) for d in dims):
        exit('ERR: invalid function parameter type(s)')

    if len(dims) < 2 or len(dims) > 3:
        return None

    dor = {}
    for sw_idx in range(num_switches):
        idx_vector = _get_switch_idx_vector(dims, sw_idx)
        switch = _get_switch_name(switch_prefix, idx_vector)
        dor[switch] = {'dimList': []}

    # generate list with DOR switches
    for sw_idx in range(num_switches):
        idx_vector = _get_switch_idx_vector(dims, sw_idx)
        switch = _get_switch_name(switch_prefix, idx_vector)

        # get neightbor nodes in all 2 (or 3) axis with current position +/- 1
        down_vec = idx_vector[:]
        down_vec[0] = (down_vec[0] + 1) % dims[0]
        down_sw = _get_switch_name(switch_prefix, down_vec)
        up_vec = idx_vector[:]
        up_vec[0] = (up_vec[0] - 1) % dims[0]
        up_sw = _get_switch_name(switch_prefix, up_vec)

        right_vec = idx_vector[:]
        right_vec[1] = (right_vec[1] + 1) % dims[1]
        right_sw = _get_switch_name(switch_prefix, right_vec)
        left_vec = idx_vector[:]
        left_vec[1] = (left_vec[1] - 1) % dims[1]
        left_sw = _get_switch_name(switch_prefix, left_vec)

        if len(dims) > 2:
            back_vec = idx_vector[:]
            back_vec[2] = (back_vec[2] + 1) % dims[2]
            back_sw = _get_switch_name(switch_prefix, back_vec)
            front_vec = idx_vector[:]
            front_vec[2] = (front_vec[2] - 1) % dims[2]
            front_sw = _get_switch_name(switch_prefix, front_vec)

        if wrap_around_links:
            dor[switch]['dimList'] = [down_sw, up_sw, right_sw, left_sw]
            if len(dims) > 2:
                dor[switch]['dimList'].extend([back_sw, front_sw])
        else:
            if down_vec[0] < dims[0]:
                dor[switch]['dimList'].append(down_sw)
            if up_vec[0] >= 0:
                dor[switch]['dimList'].append(up_sw)
            if right_vec[1] < dims[1]:
                dor[switch]['dimList'].append(right_sw)
            if left_vec[1] >= 0:
                dor[switch]['dimList'].append(left_sw)
            if len(dims) > 2:
                if back_vec[2] < dims[2]:
                    dor[switch]['dimList'].append(back_sw)
                if front_vec[2] >= 0:
                    dor[switch]['dimList'].append(front_sw)

    return dor


def _generate_mesh_and_torus(
        args, dims, gap=0, wrap_around_links=True, multi_link=False, include_supplement_material=False):
    if not isinstance(args, dict) or not all(isinstance(d, int) for d in dims):
        exit('ERR: invalid function parameter type(s)')
    elif gap > 0 and wrap_around_links:
        exit('ERR: invalid parameter combination, no ExpressTorus support')

    topology = {}
    ports = {}

    do_port_matching = args.get('include_ports')
    if do_port_matching is None:
        do_port_matching = False
    elif not isinstance(do_port_matching, bool):
        exit('ERR: include_ports must be of type bool')

    num_terminals, num_ports_per_terminal, terminal_prefix, \
        num_ports_per_switch, switch_prefix = _extract_basic_config(args)

    num_switches = 1
    for dim in dims:
        num_switches *= dim

    if num_terminals == -1:
        num_terminals = num_switches

    if num_ports_per_switch == -1:
        num_ports_per_switch = 2 * \
            len(dims) + int(ceil(num_terminals / num_switches))
        if gap > 0:
            num_ports_per_switch += \
                sum([(int(ceil((d - 2) / gap)) - 1) for d in dims if d > 2])

    # generate all switches in the integer lattice
    sw_guid = pow(2, 33)
    for sw_idx in range(num_switches):
        idx_vector = _get_switch_idx_vector(dims, sw_idx)
        switch = _get_switch_name(switch_prefix, idx_vector)
        topology[switch] = {'guid': sw_guid,
                            'radix': num_ports_per_switch,
                            'adj': [],
                            'is_switch': True}
        ports[switch] = num_ports_per_switch
        # there's no real root/spine for tori/meshes, so use first sw if needed
        if sw_idx == 0:
            topology[switch]['fake_root_switch'] = True
        sw_guid -= 26

    # generate terminals and connect them to switches (round-robin)
    set_term = 0
    while set_term < num_terminals:
        for sw_idx in range(num_switches):
            idx_vector = _get_switch_idx_vector(dims, sw_idx)
            switch = _get_switch_name(switch_prefix, idx_vector)
            terminal = _get_terminal_name(terminal_prefix, set_term)
            topology[terminal] = {
                'guid': 4 * (set_term + 1),
                'radix': num_ports_per_terminal,
                'adj': []}
            for x in range(num_ports_per_terminal):
                topology[switch]['adj'].append(terminal)
                ports[switch] -= 1
            set_term += 1
            if set_term >= num_terminals:
                break

    # connect all switches (multiple times if possible)
    if wrap_around_links:
        min_ports_needed_for_round = 2 * len(dims)
    else:
        min_ports_needed_for_round = 2 * \
            len([d for d in dims if d > 2]) + len([d for d in dims if d < 3])
        if gap > 0:
            min_ports_needed_for_round += \
                sum([(int(ceil((d - 2) / gap)) - 1) for d in dims if d > 2])

    while True:
        for sw_idx in range(num_switches):
            idx_vector_1 = _get_switch_idx_vector(dims, sw_idx)
            switch1 = _get_switch_name(switch_prefix, idx_vector_1)
            for i in range(len(dims)):
                idx_vector_2 = idx_vector_1[:]
                idx_vector_2[i] = (idx_vector_2[i] + 1) % dims[i]
                if not wrap_around_links and idx_vector_2[
                        i] == 0 or dims[i] == 1:
                    continue
                switch2 = _get_switch_name(switch_prefix, idx_vector_2)
                topology[switch1]['adj'].append(switch2)
                ports[switch1] -= 1
                ports[switch2] -= 1
            # is the ExpressMesh requested?
            if gap > 0:
                for i in range(len(dims)):
                    for m in range(1, dims[i]):
                        idx_vector_2 = idx_vector_1[:]
                        idx_vector_2[i] += 1 + m * gap
                        if idx_vector_2[i] >= dims[i]:
                            continue
                        switch2 = _get_switch_name(switch_prefix, idx_vector_2)
                        topology[switch1]['adj'].append(switch2)
                        ports[switch1] -= 1
                        ports[switch2] -= 1
        if not multi_link:
            break
        if min(ports.values()) < min_ports_needed_for_round:
            break

    # is the topology valid?
    if not _topology_is_valid(topology, ports, switch_prefix):
        exit('ERR: not enough ports per switch for all links and terminals')
    elif do_port_matching:
        _topology_port_matching(topology)

    if include_supplement_material:
        supplement_material = {}

        torus2qos_routing_config = _get_osm_torus2qos_routing_config(
            topology,
            dims,
            wrap_around_links,
            switch_prefix,
            num_ports_per_switch)
        dor_routing_config = _get_osm_dor_routing_config(
            topology, dims, num_switches, switch_prefix, wrap_around_links)

        if torus2qos_routing_config:
            supplement_material[
                'osm_torus2qos_input'] = torus2qos_routing_config
        if dor_routing_config:
            supplement_material['osm_dor_input'] = dor_routing_config

        return (topology, supplement_material)

    return topology


def generate_topology(args={}):
    """
    default 3x2 torus with single link between adjacent switches
    """
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    if args.get('__argparse_used__'):
        dims, multi_link, incl_suppl = _get_local_options(args)
    else:
        dims = args.get('dimensions')
        if dims is None:
            dims = [3, 2]
        elif not all(isinstance(d, int) for d in dims) or min(dims) < 0:
            exit('ERR: dimensions must be a list of positive integers')

        multi_link = args.get('multi_link')
        if multi_link is None:
            multi_link = False
        elif not isinstance(multi_link, bool):
            exit('ERR: multi_link must be of type bool')

        incl_suppl = args.get('create_supplement_material')
        if incl_suppl is None:
            incl_suppl = False
        elif not isinstance(incl_suppl, bool):
            exit('ERR: create_supplement_material must be of type bool')

    if len(dims) < 1:
        exit('ERR: at least one positive dimension size must be given')
    if min(dims) < 2:
        stderr.write('WRN: sizes < 2 for torus dimensions will be ignored\n')
        dims = [dim for dim in dims if dim > 1]
    dims.sort(reverse=True)

    return _generate_mesh_and_torus(
        args, dims, 0, True, multi_link, incl_suppl)


if __name__ == "__main__":
    exit('ERR: not meant to be executed as stand-alone')
