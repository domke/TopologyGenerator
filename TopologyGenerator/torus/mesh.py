#!/usr/bin/env python3

from sys import exit, stderr
from argparse import _SubParsersAction
from ..common import _topology_is_valid, _extract_basic_config
from ..common import _get_terminal_name
from .torus import _generate_mesh_and_torus


def _set_local_options(arg_subparser):
    if not isinstance(arg_subparser, _SubParsersAction):
        exit('ERR: needs to be called with existing SubArgumentParser object')

    arg_parser = arg_subparser.add_parser(
        'mesh',
        description='n-dimensional mesh topology')
    arg_parser.add_argument(
        '--dim',
        dest='__mesh_dims__',
        type=int,
        nargs='+',
        help='list of the mesh sizes in each dimension [default: 3 3 (for 3x3-mesh)]',
        metavar='N',
        default=[3, 3])
    arg_parser.add_argument(
        '--ml',
        dest='__mesh_ml__',
        action='store_true',
        help='enable multi-link configuration between switches to fillup empty ports',
        default=False)
    arg_parser.add_argument(
        '--ext',
        dest='__mesh_extended_material__',
        action='store_true',
        help='generate supplement material (i.e., DOR/Torus2QoS routing input for OpenSM; only applies to 2D and 3D meshes)',
        default=False)


def _get_local_options(args):
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    dims = args.get('__mesh_dims__')
    multi_link = args.get('__mesh_ml__')
    include_suppl = args.get('__mesh_extended_material__')

    return (dims, multi_link, include_suppl)


def generate_topology(args={}):
    """
    default 3x3 mesh with single link between adjacent switches
    """
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')

    if args.get('__argparse_used__'):
        dims, multi_link, incl_suppl = _get_local_options(args)
    else:
        dims = args.get('dimensions')
        if dims is None:
            dims = [3, 3]
        elif not all(isinstance(d, int) for d in dims) or min(dims) < 0:
            exit('ERR: dimensions must be a list of positive integers')

        multi_link = args.get('multi_link')
        if multi_link is None:
            multi_link = False
        elif not isinstance(multi_link, bool):
            exit('ERR: multi_link must be of type bool')

        incl_suppl = args.get('create_supplement_material')
        if incl_suppl is None:
            incl_suppl = False
        elif not isinstance(incl_suppl, bool):
            exit('ERR: create_supplement_material must be of type bool')

    if len(dims) < 1:
        exit('ERR: at least one positive dimension size must be given')
    if min(dims) < 2:
        stderr.write('WRN: sizes < 2 for mesh dimensions will be ignored\n')
        dims = [dim for dim in dims if dim > 1]
    dims.sort(reverse=True)

    return _generate_mesh_and_torus(args, dims, 0, False, multi_link, incl_suppl)


if __name__ == "__main__":
    exit('ERR: not meant to be executed as stand-alone')
